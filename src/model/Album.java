package model;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import controller.PhoenixCore;


/**
 * Stellt ein Album dar
 * @author artur
 *
 */
public class Album {
	private static PreparedStatement countChilds = null;
	int id;
	
	private String name;
	private String artistName;
	private int artistID;
	private Album() {
		if (countChilds==null) {
			try {
				Album.countChilds = PhoenixCore.DBCON.prepareOnLib("select count(id) as anz from tracks where album=?");
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
	}
	public Album(int id, String name){
		this();
		this.id = id;
		this.name=name;
	}
	public Album(int id, String name, int artistID, String artistName){
		this(id, name);
		this.artistID = artistID;
		this.artistName=artistName;
	}
	@Override
	public String toString() {
		return name.equals("")||name.equals("NULL")?"Unbekannt ("+getTrackCount()+")":id==0?name:name+" ("+getTrackCount()+")";
	}
	
	public int getTrackCount() {
		int count=0;
		try {
			Album.countChilds.setInt(1, id);
			ResultSet rs = Album.countChilds.executeQuery();
			rs.next();
			count = rs.getInt(1);
			rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return count;
	}
	
	public int getId() {
		return id;
	}
	public String getName() {
		return name;
	}
	public String getArtistName() {
		return artistName;
	}
	public void setArtistName(String artistName) {
		this.artistName = artistName;
	}
	public int getArtistID() {
		return artistID;
	}
	public void setArtistID(int artistID) {
		this.artistID = artistID;
	}
}
