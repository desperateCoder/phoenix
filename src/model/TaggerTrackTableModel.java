package model;

import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.table.AbstractTableModel;

import org.jaudiotagger.audio.exceptions.CannotReadException;
import org.jaudiotagger.audio.exceptions.CannotWriteException;
import org.jaudiotagger.audio.exceptions.InvalidAudioFrameException;
import org.jaudiotagger.audio.exceptions.ReadOnlyFileException;
import org.jaudiotagger.tag.TagException;

import controller.PhoenixCore;
import controller.TagIO;
import controller.listener.TagChangeListener;
/**
 * Im Tagget befindet sich diese Tabelle, die die reingeschobenen tracks aufnimmt und in geeigneter form darstellt.
 * das beinhaltet den Dateinamen und einen status. der status zeigt die vorhandenen tags an und wenn ein track geändert wurde,
 * auch ein entsprechendes symbol, dass man weiß, dass man speichern muss..
 * @author artur
 *
 */
@SuppressWarnings("serial")
public class TaggerTrackTableModel extends AbstractTableModel implements AbstractTrackTableModel, TagChangeListener{
	//Status-Konstanten
	public static final Byte V1_TAGGED=1;
	public static final Byte V2_TAGGED=2;
	public static final Byte BOTH_TAGGED=3;
	public static final Byte UNSAVED=0;
	private String columnNames[] = {"S", "Dateiname"};
	private ArrayList<Object[]> tracks;
	PreparedStatement artistExists=null;
	PreparedStatement albumExists=null;
	PreparedStatement overwrite=null;
	PreparedStatement cleanArtists=null;
	PreparedStatement cleanAlbums=null;
	public TaggerTrackTableModel() { 
		tracks = new ArrayList<Object[]>();
		try {
			artistExists=PhoenixCore.DBCON.prepareOnLib("select id from artists where name=?");
			albumExists=PhoenixCore.DBCON.prepareOnLib("select id from albums where name=? and artist=?");
			overwrite=PhoenixCore.DBCON.prepareOnLib("update tracks set track=?, name=?, album=?, genre=? where id=?");
			cleanArtists=PhoenixCore.DBCON.prepareOnLib("delete from artists where id not in (select distinct artist from albums)");
			cleanAlbums=PhoenixCore.DBCON.prepareOnLib("delete from albums where id not in (select distinct album from tracks)");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	public int getRowCount() {
		return tracks.size();
	}
	public int getColumnCount() {
		return columnNames.length;
	}
	public Object getValueAt(int rowIndex, int columnIndex) {
		return columnIndex==0?tracks.get(rowIndex)[columnIndex]:((Track)tracks.get(rowIndex)[columnIndex]).getPath().getFileName().toString();
	}
	public Track getTrackAt(int index){
		return (Track)tracks.get(index)[1];
	}	
	public String getColumnName(int col) {
		return columnNames[col];
	}
	public void addTracks(Track[] tracks){
		Object o[];
		for (Track track : tracks) {
			o = new Object[2];
			o[0] = TaggerTrackTableModel.BOTH_TAGGED;
			o[1] = track;
			this.tracks.add(o);
		}
		fireTableDataChanged();
	}
	public Library getLib() {
		List<Track> trackList = new ArrayList<Track>();
		for (int i = 0; i < tracks.size(); i++) {
			trackList.add((Track)tracks.get(i)[1]);
		}
		return new Library(trackList);
	}
	
	/**
	 * Liefert Die Tracks an den übergebenen Indizes
	 * @param sel Array der selektierten indizes
	 */
	public Track[] getSelectedTracks(int[] sel) {
		Track t[] = new Track[sel.length];
		for (int i = 0; i < sel.length; i++) {
			t[i] = (Track) tracks.get(sel[i])[1];
		}
		return t;
	}
	/**
	 * wird aufgerufen, wenn tags geändert wurden
	 */
	public void tagsChanged(List<Track> tracks, int[] selectionIndex) {
		for (int i = 0; i < selectionIndex.length ; i++) {
			Object arr[] ={TaggerTrackTableModel.UNSAVED, tracks.get(i)};
			this.tracks.set(selectionIndex[i], arr);
		}
		fireTableDataChanged();
	}
	/**
	 * schreibt die aktuellen Tracks in die DB
	 */
	public void writeTags(){
		//tags reinschreiben
		for (int i = 0; i<tracks.size(); i++) {
			if (tracks.get(i)[0]==TaggerTrackTableModel.UNSAVED) {
				Track t = (Track) tracks.get(i)[1];
			try {
				TagIO.writeTags(t);
			} catch (CannotReadException e) {
				e.printStackTrace();continue;
			} catch (IOException e) {
				e.printStackTrace();continue;
			} catch (TagException e) {
				e.printStackTrace();continue;
			} catch (ReadOnlyFileException e) {
				e.printStackTrace();continue;
			} catch (InvalidAudioFrameException e) {
				e.printStackTrace();continue;
			} catch (CannotWriteException e) {
				e.printStackTrace();continue;
			}
			overwriteTagsForTagInDB(t);
			}
		}
		//überflüssige artists und albums aus der db entfernen
		try {
			cleanAlbums.execute();
			cleanArtists.execute();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	private void overwriteTagsForTagInDB(Track t){
		int artist=0;
		int album=0;
		try {
			//artist scho da?
			artistExists.setString(1, t.getArtistName());
			ResultSet rs = artistExists.executeQuery();
			if (rs.next()) {
				//wenn id schon da, die nehmen
				artist=rs.getInt(1);
				rs.close();
			}else {
				rs.close();
				//sonst einfügen
				artist=PhoenixCore.DBCON.insertOnLib("insert into artists (name) values ("+t.getArtistName()+")");
			}
			//album scho da?
			albumExists.setString(1, t.getAlbumName());
			albumExists.setInt(2, artist);
			rs=albumExists.executeQuery();
			if (rs.next()) {
				//wenn id schon da, die nehmen
				album=rs.getInt(1);
				rs.close();
			}else {
				rs.close();
				//sonst einfügen
				album=PhoenixCore.DBCON.insertOnLib("insert into artists (name) values ("+t.getArtistName()+")");
			}
			int trackNr=0;
			try {
				trackNr=Integer.parseInt(t.getTrack());
			} catch (Exception e) {
				e.printStackTrace();
			}
			// und track überschreiben
			overwrite.setInt(1, trackNr);
			overwrite.setString(2, t.getTitle());
			overwrite.setInt(3, album);
			overwrite.setString(4, t.getGenre());
			overwrite.setInt(5, t.getID());
			overwrite.execute();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
