package model;

import java.util.List;
import java.util.Map;

import javax.swing.table.AbstractTableModel;

import controller.interfaces.Reorderable;

/**
 * TableModel für die Track-Tabelle.
 * 
 * @author Artur Dawtjan
 * 
 */
@SuppressWarnings("serial")
public class TrackTableModel extends AbstractTableModel implements Reorderable,
		AbstractTrackTableModel {

	public static final char SORT_ASC = 1;
	public static final char SORT_DESC = 2;

	public static final char COL_PLAY = 10;
	public static final char COL_IS_SELECTED = 11;
	public static final char COL_NR = 12;
	public static final char COL_TITLE = 13;
	public static final char COL_ALBUM = 14;
	public static final char COL_ARTIST = 15;
	public static final char COL_LENGTH = 16;
	public static final char COL_GENRE = 17;

	Library lib;
	private int playing;
	private Map<Integer, String> colMap;
	private boolean isCheckBoxShown;
	private boolean tracksAddable;

	public TrackTableModel(Library lib, Map<Integer, String> colMap,
			boolean showCheckBox) {
		this(lib, colMap, showCheckBox, false);
	}

	public TrackTableModel(Library lib, Map<Integer, String> colMap) {
		this(lib, colMap, true);
	}

	public TrackTableModel(Library lib, Map<Integer, String> colMap,
			boolean showCheckBox, boolean tracksAddable) {
		this.lib = lib;
		this.colMap = colMap;
		this.isCheckBoxShown = showCheckBox;
		this.tracksAddable = tracksAddable;
	}


	public int getColumnCount() {
		return colMap.size();
	}

	public int getRowCount() {
		return lib.getTrackCount();
	}

	public String getColumnName(int col) {
		return colMap.get(col);
	}

	public Object getValueAt(int row, int col) {
		if (row >= lib.getTrackCount()) {
			return null;
		}
		Track t = lib.getTrackAt(row);
		if (isCheckBoxShown) {

			switch (col) {
			case 0:
				return t.isPlaying()?"I>":"";
			case 1:
				return t.getTrack();
			case 2:
				return (Boolean) t.isChecked();
			case 3:
				return t.getTitle();
			case 4:
				return t.getArtistName();
			case 5:
				return t.getAlbumName();
			case 6:
				return t.getGenre();
			case 7:
				return t.getStringLength();
			}
		} else {

			switch (col) {
			case 0:
				return t.isPlaying()?"I>":"";
			case 1:
				return t.getTrack();
			case 2:
				return t.getTitle();
			case 3:
				return t.getArtistName();
			case 4:
				return t.getAlbumName();
			case 5:
				return t.getGenre();
			case 6:
				return t.getStringLength();
			}
		}
		return null;
	}

	/**
	 * Liefert den Track an übergebenem index
	 * 
	 * @param index
	 * @return Track, welcher hinter der zeile steht
	 */
	public Track getTrackAt(int index) {
		return lib.getTrackAt(index);
	}

	/*
	 * JTable uses this method to determine the default renderer/ editor for
	 * each cell. If we didn't implement this method, then the last column would
	 * contain text ("true"/"false"), rather than a check box.
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public Class getColumnClass(int c) {
		return getValueAt(0, c).getClass();
	}

	public boolean isCellEditable(int row, int col) {
		if (col != 2) {
			return false;
		} else {
			return true;
		}
	}

	public void setValueAt(Object value, int row, int col) {
		if (value instanceof Boolean) {
			lib.setChecked((Boolean) value, lib.getTrackAt(row).getID(), row);
			fireTableCellUpdated(row, col);
		}else if (col==0) {
			if ("".equals(value)) {
				lib.getTrackAt(row).setPlaying(false);
			}else lib.getTrackAt(row).setPlaying(true);
		}
	}

	public void setPlaying(Track t, int index) {
		if (index >= 0) {
			if (t.equals(lib.getTrackAt(index))) {
				setValueAt("", playing, 0);
				playing = index;
				setValueAt("I>", index, 0);
			}
		}
		
	}

	/**
	 * liefert die entsprechenden tracks zu den indizes
	 * 
	 * @param selectedRows
	 *            indizes der tracks im model
	 * @return die tracks an selectedRows positionen
	 */
	public Track[] getTracksAt(int[] selectedRows) {
		Track[] tracks = new Track[selectedRows.length];
		for (int i = 0; i < tracks.length; i++) {
			tracks[i] = lib.getTrackAt(selectedRows[i]); // einpacken
		}
		return tracks;
	}

	/**
	 * @return Alle Tracks im model
	 */
	public List<Track> getAllTracks() {
		return lib.getTracks();
	}

	/**
	 * setzt die Aktuellen Titel für die Lib
	 */
	public void setTracksInLib(List<Track> tracks) {
		lib.setTracks(tracks);
	}

	/**
	 * Fuegt die Tracks am Ende der Liste hinzu. vor allem für die Queue-view.
	 * 
	 * @param tracks
	 *            Track[]
	 */
	public void addTracks(Track[] tracks) {
		// nur wenns im queue-modus ist, werden die tracks hinzugefuegt.
		if (this.tracksAddable) {
			lib.addTracks(tracks);
			fireTableDataChanged();
			Runtime.getRuntime().gc();
		}
	}

	public void reorder(int fromIndex, int toIndex) {
		lib.reorder(fromIndex, toIndex);
		fireTableDataChanged();
	}

	public Library getLib() {
		return lib;
	}

	@Override
	public void fireTableDataChanged() {
		super.fireTableDataChanged();
	}

	/**
	 * Sortiert die übergebene Spalte. die richtung wird durch die übergebene
	 * klassenkonstante gesteuert
	 * 
	 * @param columnConstant
	 * @param sortAsc
	 */
	public void sort(char columnConstant, char sortDirection) {
		if (columnConstant != TrackTableModel.COL_NR) {
			lib.sort(columnConstant, sortDirection);
			fireTableDataChanged();
		}
	}

	public void quickSearch(String text) {
		lib.quickSearch(text);
		fireTableDataChanged();
	}

	public void removeTracksAt(int[] rowsToRemove) {
		for (int i = 0; i < rowsToRemove.length; i++) {
			lib.removeTrackAt(rowsToRemove[i]);
		}
	}

	public void shuffleTracks() {
		lib.shuffle();
		fireTableDataChanged();
	}
}
