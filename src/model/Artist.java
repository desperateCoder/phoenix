package model;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import controller.PhoenixCore;

/**
 * oberste hierarchie-ebene des musik-baums
 * 
 * @author Artur Dawtjan
 *
 */
public class Artist {
	private static PreparedStatement countChilds = null;
	private int id = 0;
	
	private String name;
	List<Album> albums;
	
	private Artist() {
		if (countChilds==null) {
			try {
				Artist.countChilds = PhoenixCore.DBCON.prepareOnLib("select count(t.id) as anz from tracks t  where t.album in (select a.id from albums a where a.artist = ?)");
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
	}
	/**
	 * Konstruktor, falls interpret noch nicht eingetragen ist. dieser wird in die DB eingetragen.
	 * 
	 * @param name des Interpreten
	 */
	public Artist(String name){
		this();
		this.name = name;
	}
	
	/**
	 * Konstruktor, falls interpret bereits eingetragen ist.
	 * @param id des Interpreten aus der db
	 * @param name des Interpreten
	 */
	public Artist(int id, String name){
		this(name);
		this.id = id;
	}
	@Override
	public String toString() {
		return name.equals("")||name.equals("NULL")?"Unbekannt ("+getAlbumCount()+")":id==0?name:name+" ("+getAlbumCount()+")";
	}
	public int getAlbumCount() {
		int count=0;
		try {
			Artist.countChilds.setInt(1, id);
			ResultSet rs = Artist.countChilds.executeQuery();
			rs.next();
			count = rs.getInt(1);
			rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return count;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Album> getAlbums() {
		return albums;
	}

	public void setAlbums(List<Album> albums) {
		this.albums = albums;
	}

}
