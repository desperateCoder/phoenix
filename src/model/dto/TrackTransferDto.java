package model.dto;

import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.File;
import java.io.IOException;
import java.util.Vector;

import model.Track;
/**
 * Das Transferable-Objekt, welches die selektierten Tracks aus der view beinhaltet.
 * @author Artur Dawtjan
 *
 */
public class TrackTransferDto implements Transferable {
	Track[] tracks;

	/**
	 * Einfacher konstruktor
	 * @param tracks, die dieses DTO beinhalten soll.
	 */
	public TrackTransferDto(Track[] tracks) {
		this.tracks = tracks;
	}
	
	/**
	 * liefert die unterstuetzten datentypen.
	 */
	public DataFlavor[] getTransferDataFlavors() {
		DataFlavor[] df = new DataFlavor[2];
		try {
		df[0]= new DataFlavor(DataFlavor.javaJVMLocalObjectMimeType +
				";class=\""+ TrackTransferDto.class.getName() + "\"");
		df[1]=DataFlavor.javaFileListFlavor;
		}catch (ClassNotFoundException e){
			e.printStackTrace();
		}
		return df;
	}

	public boolean isDataFlavorSupported(DataFlavor flavor) {
		return false;
	}

	public Object getTransferData(DataFlavor flavor)
			throws UnsupportedFlavorException, IOException {
		 if( flavor.equals(DataFlavor.javaFileListFlavor)) {//Files werden verlangt
			 Vector<File> fileList = new Vector<File>();
			 for (int i = 0; i < tracks.length; i++) {
				fileList.add(new File(tracks[i].getPath().toString()));
			}
	         return fileList ;
		 } else// Tracks werden verlangt.
			try {
				if (flavor.equals(new DataFlavor(DataFlavor.javaJVMLocalObjectMimeType + ";class=\""
												+ TrackTransferDto.class.getName() + "\""))) {
					return this;
				}
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
	      throw new UnsupportedFlavorException(flavor);
	}

	public Track[] getTracks() {
		return tracks;
	}

}
