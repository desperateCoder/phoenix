package model;

import java.io.File;
import java.nio.file.Path;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import controller.PhoenixCore;

public class Track {
	private static PreparedStatement getArtistID = null;
	private static PreparedStatement getArtistName = null;
	private static PreparedStatement getAlbumID = null;
	private static PreparedStatement getAlbumName = null;
	private static PreparedStatement getGenre = null;
	private static PreparedStatement getTrack = null;
	private static PreparedStatement getLength = null;
	private static PreparedStatement getPath = null;
	private static PreparedStatement getTitle = null;
	private static PreparedStatement getIsChecked = null;
//	private static PreparedStatement deleteFromDB = null;
	
	int id =-1;
	private boolean isPlaying = false;
	public Track(String track, String artist, String album, String title,
			String genre, int length, Path p){
		
	}
	/**
	 * einfacher konstruktor, spart arbeitsspeicher indem er auf Anfrage aus der DB liest
	 * @param id in der DB
	 */
	public Track(int id){
		this.id = id;
		if (Track.getArtistID==null) {
			try {
				Track.getArtistName = PhoenixCore.DBCON.prepareOnLib("select ar.name from albums a inner join tracks t on a.id = t.album inner join artists ar on ar.id = a.artist where t.id=?");
				Track.getAlbumName = PhoenixCore.DBCON.prepareOnLib("select a.name from albums a inner join tracks t on a.id = t.album where t.id=?");
				Track.getAlbumID = PhoenixCore.DBCON.prepareOnLib("select a.id from albums a inner join tracks t on a.id = t.album where t.id=?");
				Track.getArtistID = PhoenixCore.DBCON.prepareOnLib("select ar.id from albums a inner join tracks t on a.id = t.album inner join artists ar on ar.id = a.artist where t.id=?");
				Track.getGenre = PhoenixCore.DBCON.prepareOnLib("select genre from tracks where id=?");
				Track.getTrack = PhoenixCore.DBCON.prepareOnLib("select track from tracks where id=?");
				Track.getLength = PhoenixCore.DBCON.prepareOnLib("select length from tracks where id=?");
				Track.getPath = PhoenixCore.DBCON.prepareOnLib("select path from tracks where id=?");
				Track.getTitle = PhoenixCore.DBCON.prepareOnLib("select name from tracks where id=?");
				Track.getIsChecked = PhoenixCore.DBCON.prepareOnLib("select checked from tracks where id=?");
//				Track.deleteFromDB = PhoenixCore.DBCON.prepareOnLib("delete from tracks where id=?");
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
	}
	
	/**
	 * wandelt den intwert in h:mm:ss um
	 * 
	 * @param length
	 * @return length in string
	 */
	public static String intToStringLength(int length) {
		String l = "";
		int h = 0, m = 0, s = 0, tmp = 0;
		s = length % 60; // sekunden
		tmp = length - s;
		m = tmp / 60;// minuten
		if (m >= 60) {// falls ne stunde erreicht wurde
			h = m / 60;
			m = m - h * 60;
		}
		if (h != 0) {// falls stunde erreicht
			l += h + ":";// mit ausgeben
		}
		l += h == 0 ? m + ":" : String.format("%02d:", m); // führende nullen
															// für minuten nur
															// bei vorhandenen
															// stunden
		l += String.format("%02d", s);
		return l;
	}



	public String getStringLength() {
		String l = "";
		int h = 0, m = 0, s = 0, tmp = 0;
		int length = getLength();
		s = length % 60; // sekunden
		tmp = length - s;
		m = tmp / 60;// minuten
		if (m >= 60) {// falls ne stunde erreicht wurde
			h = m / 60;
			m = m - h * 60;
		}
		if (h != 0) {// falls stunde erreicht
			l += h + ":";// mit ausgeben
		}
		l += h == 0 ? m + ":" : String.format("%02d:", m); // führende nullen
															// für minuten nur
															// bei vorhandenen
															// stunden
		l += String.format("%02d", s);
		return l;
	}

	// #########################>- GETTER & SETTER -<###########################

	public boolean isPlaying() {
		return isPlaying;
	}

	public void setPlaying(boolean isPlaying) {
		this.isPlaying = isPlaying;
	}

	public int getID() {
		return this.id;
	}

	private ResultSet execute(PreparedStatement stmt){
		try {
			stmt.setInt(1, id);
			return stmt.executeQuery();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	private int getFirstIntFromStatement(PreparedStatement stmt){
		ResultSet rs = execute(stmt);
		try {
			if(rs.next()){
				int item = rs.getInt(1);
				rs.close();
				return item;
			}
			rs.close();
				
		} catch (SQLException e) {
			e.printStackTrace();
		}
		try {
			rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return -1;
	}
	private String getFirstStringFromStatement(PreparedStatement stmt){
		ResultSet rs = execute(stmt);
		try {
			if(rs.next()){
				String s = rs.getString(1);
				rs.close();
				return s;
			}
			rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return "";
	}
	private boolean getFirstBooleanFromStatement(PreparedStatement stmt){
		ResultSet rs = execute(stmt);
		try {
			if(rs.next()){
				boolean b = rs.getBoolean(1);
				rs.close();
				return b;
			}
			rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}
	public int getArtistID() {
		return getFirstIntFromStatement(Track.getArtistID);
	}

	public void setArtistID(int artistID) {
		//this.artistID = artistID;
	}

	public int getAlbumID() {
		return getFirstIntFromStatement(Track.getAlbumID);
	}

	public void setAlbumID(int albumID) {
		//this.albumID = albumID;
	}

	public String getArtistName() {
		String artistName = getFirstStringFromStatement(Track.getArtistName);
		return artistName.equals(null)?"":artistName;
	}

	public void setArtistName(String artistName) {
		//this.artistName = artistName;
	}

	public String getAlbumName() {
		return getFirstStringFromStatement(Track.getAlbumName);
	}

	public void setAlbumName(String albumName) {
		//this.albumName = albumName;
	}

	public String getTrack() {
		return getFirstStringFromStatement(Track.getTrack);
	}

	public void setTrack(String track) {
		//this.track = track;
	}

	public String getTitle() {
		return getFirstStringFromStatement(Track.getTitle);
	}

	public void setTitle(String title) {
		//this.title = title;
	}

	public String getGenre() {
		return getFirstStringFromStatement(Track.getGenre);
	}

	public void setGenre(String genre) {
		//this.genre = genre;
	}

	public int getLength() {
		return getFirstIntFromStatement(Track.getLength);
	}

	public void setLength(int length) {
		//this.length = length;
	}

	public Path getPath() {
		return new File(getFirstStringFromStatement(Track.getPath)).toPath();
	}

	public void setPath(Path path) {
		//this.path = path;
	}

	public boolean isChecked() {
		return getFirstBooleanFromStatement(Track.getIsChecked);
	}

	public void setChecked(boolean checked) {
	}


	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Track) {
			Track t = (Track) obj;
			return getPath().equals(t.getPath());
		}
		return false;
	}

	@Override
	public String toString() {
		return getTitle() + " von " + getArtistName();
	}

	/**
	 * vergleicht den übergebenen track mit this. 
	 * @param t Track, mit dem this verglichen werden soll
	 * @param columnConstant Nach welchem Kriterium sortiert werden soll. zu finden in der TrackTableModel Klasse
	 * @return true, wenn this größer und false, wenn this kleiner oder gleich t ist.
	 */
	public boolean compareTo(Track t, char columnConstant) {
		try {
			switch (columnConstant) {
			case TrackTableModel.COL_TITLE:
				return getTitle().compareToIgnoreCase(t.getTitle())>0;
			case TrackTableModel.COL_ALBUM:
				return compareAlbum(t);
			case TrackTableModel.COL_ARTIST:
				return compareArtist(t);
			case TrackTableModel.COL_LENGTH:
				return ((Integer)getLength()).compareTo(t.getLength())>0;
			case TrackTableModel.COL_GENRE:
				return getGenre().compareToIgnoreCase(t.getGenre())>0;
			case TrackTableModel.COL_IS_SELECTED:
				return isChecked()&&!t.isChecked()?true:false;
				
			default:
				return false;
			}
		} catch (StackOverflowError e) {
			System.out.println("###ERROR: StackOverflow! Sortier-Rekursion wird hier unterbrochen.###");
			//e.printStackTrace();
			return false;
		}
	}
	private boolean compareArtist(Track t) {
		int temp = this.getArtistName().compareToIgnoreCase(t.getArtistName());
		if (temp>0) {
			return true;
		}else if (temp<0) {
			return false;
		}else {// wenn artist gleich, nach album sortieren.
			return compareAlbum(t);
		}
	}

	private boolean compareAlbum(Track t){
		int temp = getAlbumName().compareToIgnoreCase(t.getAlbumName());
		if (temp==0) {
			int thisTrack;
			int tTrack;
			try {
				thisTrack = Integer.parseInt(getTrack());
			} catch (NumberFormatException e) {
				return true;
			}
			try {
				tTrack = Integer.parseInt(t.getTrack());
			} catch (Exception e) {
				return false;
			}
			return thisTrack > tTrack;
		}else if(temp>0){
			return true;
		}
		return false;
	}
	
	public void deleteFromDB() {
		
	}

}
