package model;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import controller.PhoenixCore;


public class PlaylistTrack extends Track {

	private static PreparedStatement getIsCheckedInPlaylist = null;
	private static PreparedStatement deleteFromDB = null;
	int playlistID=-1;
	/**
	 * Konstruktor fuer tracks in einer playlist
	 * @param id
	 * @param playlistID
	 */
	public PlaylistTrack(int id, int playlistID) {
		super(id);
		this.playlistID=playlistID;
		try {
			if (PlaylistTrack.getIsCheckedInPlaylist==null) {
				PlaylistTrack.getIsCheckedInPlaylist = PhoenixCore.DBCON.prepareOnLib("select checked from playlist_track where listid=? and trackid=?");
				PlaylistTrack.deleteFromDB = PhoenixCore.DBCON.prepareOnLib("delete from playlist_track where listid=? and trackid=?");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	
	/**
	 * verraet, ob track innerhalb der übergebenen Playlist cecked ist, oder nicht.
	 * @param playlistID
	 * @return isChecked
	 */
	@Override
	public boolean isChecked(){
		
		try {
			PlaylistTrack.getIsCheckedInPlaylist.setInt(1, this.playlistID);
			PlaylistTrack.getIsCheckedInPlaylist.setInt(2, getID());
			ResultSet rs = PlaylistTrack.getIsCheckedInPlaylist.executeQuery();
			if(rs.next()){
				boolean b = rs.getBoolean(1);
				return b;
			}
			rs.close();
			getIsCheckedInPlaylist.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}
	
	/**
	 * Loescht die verknuepfung von track zu playlist
	 */
	@Override
	public void deleteFromDB() {
		try {
			PlaylistTrack.deleteFromDB.setInt(1, this.playlistID);
			PlaylistTrack.deleteFromDB.setInt(2, getID());
			PlaylistTrack.deleteFromDB.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}


}
