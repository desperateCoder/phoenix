package model;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import controller.PhoenixCore;

/**
 * Enthält die Einstellungen des Programms. sie werden in einer SQLite DB gespeichert.
 * @author Artur Dawtjan
 *
 */
public class Settings{
	/**
	 * beinhaltet key-value paare der einstellungen
	 */
	private Map<Integer, String> settingsMap = new HashMap<Integer, String>();
	/**
	 * DB-Anbindung
	 */
	private static final String GUI_KEY_VALUE = "SELECT key, value FROM gui order by key asc";
	private static PreparedStatement setSettingValue = null;
	private ResultSet rs;
	
	public Settings() {
		//daten aus DB holen
		//in hashmap eintragen
		rs = PhoenixCore.DBCON.executeOnSettings(Settings.GUI_KEY_VALUE);
		try {
			while (rs.next()) {
				settingsMap.put(rs.getInt("key"), rs.getString("value"));
			}
			rs.close();
			setSettingValue = PhoenixCore.DBCON.prepareOnSettings("update gui set value=? where key=?");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void setSetting(int settingKey, String value){
		try {
			Settings.setSettingValue.setString(1, value);
			Settings.setSettingValue.setInt(2, settingKey);
			Settings.setSettingValue.executeUpdate();
			settingsMap.put(settingKey, value);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	/**
	 * Liefert den aktuell gesetzten Wert für das setting
	 * @param key aus der konstanten-klasse SettingKeys
	 * @return value als string
	 */
	public String getSetting(int key){
		return settingsMap.get(key);
	}
	
	/**
	 * Liefert den aktuell gesetzten Wert für das setting als Integer-Wert
	 * @param key aus der konstanten-klasse SettingKeys
	 * @return	value als int, -1 beim fail
	 */
	public int getSettingAsInt(int key){
		int i = -1;
		try {
			String s = settingsMap.get(key);
			int pos = s.indexOf(".");
			if (pos==-1) {
				i = Integer.parseInt(s);
			} else {
				s = s.substring(0, pos);
				i = Integer.parseInt(s);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return i;
	}
	
	/**
	 * Liefert den aktuell gesetzten Wert für das setting als double-Wert
	 * @param key aus der konstanten-klasse SettingKeys
	 * @return	value als double, -1 beim fail
	 */
	public double getSettingAsDouble(int key){
		double i = -1;
		try {
			i = Double.parseDouble(settingsMap.get(key));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return i;
	}
	
	/**
	 * Liefert die Tabellenüberschriften für die Track-tabelle
	 * @param showChekBox true, wenn checkboxen ausgewählt sein sollen
	 * @return Map<Index:int, überschrift:string>
	 */
	public Map<Integer, String> getTrackTableCols(boolean showCheckBox){
		if (showCheckBox) {
			rs=PhoenixCore.DBCON.executeOnSettings("Select idx, name from cols order by idx asc");
		} else {
			rs=PhoenixCore.DBCON.executeOnSettings("Select idx, name from cols where name!='' order by idx asc");
		}
		
		Map<Integer, String> colMap = new HashMap<Integer, String>();
		try {
			int i = 0;
			while (rs.next()) {
				colMap.put(i, rs.getString("name"));
				i++;
			}
			rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return colMap;
	}
	/**
	 * Liefert die Tabellenüberschriften für die Track-tabelle
	 * bei dieser methode werden die Checkboxen in der Tabelle angezeigt.
	 * @return Map<Index:int, überschrift:string>
	 */
	public Map<Integer, String> getTrackTableCols(){
		return getTrackTableCols(true);
	}
}