package model.constants;
/**
 * Konstanten, die die Keys zur Settings-DB beinhalten.
 * @author Artur Dawtjan
 *
 */
public class SettingKeys {
	public static final int SKIN = 1;
	public static final int SPLIT_MAIN_LIBVIEW_POS = 2;
	public static final int SPLIT_ALBUM_LIBVIEW_POS = 3;
	public static final int SPLIT_FRAME_INNER=4;
	public static final int SPLIT_FRAME_MAIN=5;
	public static final int FRAME_HEIGHT=6;
	public static final int FRAME_WIDTH=7;
	public static final int FRAME_POSITION_X=8;
	public static final int FRAME_POSITION_Y=9;
	public static final int PLAYER_VOLUME=10;	
	public static final int PLAYER_MODE=11;	
}
