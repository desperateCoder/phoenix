package model.constants;
/**Konstanten zur identifizierung von gui-eingaben 
 * 
 * @author Artur Dawtjan
 *
 */
public class ActionCommands {
	public static final String PLAY = "pl";
	public static final String PAUSE = "pa";
	public static final String STOP = "st";
	public static final String NEXT = "ne";
	public static final String PREV = "pr";
	public static final String FILE_IMPORT_FOLDER = "fif";
	public static final String VOLUME_MUTE = "mut";
	public static final String VOLUME_MAX = "max";
	public static final int PLAY_LIB = -666;
	
}
