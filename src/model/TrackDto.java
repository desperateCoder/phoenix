package model;

import java.nio.file.Path;

/**
 * Stellt einen Titel dar
 * 
 * @author Artur Dawtjan
 * 
 */
public class TrackDto extends Track {
	private String artistName, albumName, // für den einfachen dto-mode
			track, title, genre;
	private int length = 0;
	private int artistID, albumID;
	int id;

	private Path path;
	private boolean checked, isPlaying = false;
//
//	/**
//	 * wandelt den intwert in h:mm:ss um
//	 * 
//	 * @param length
//	 * @return length in string
//	 */
//	public static String intToStringLength(int length) {
//		String l = "";
//		int h = 0, m = 0, s = 0, tmp = 0;
//		s = length % 60; // sekunden
//		tmp = length - s;
//		m = tmp / 60;// minuten
//		if (m >= 60) {// falls ne stunde erreicht wurde
//			h = m / 60;
//			m = m - h * 60;
//		}
//		if (h != 0) {// falls stunde erreicht
//			l += h + ":";// mit ausgeben
//		}
//		l += h == 0 ? m + ":" : String.format("%02d:", m); // führende nullen
//															// für minuten nur
//															// bei vorhandenen
//															// stunden
//		l += String.format("%02d", s);
//		return l;
//	}

	/**
	 * DTO-modus constructor, nur für das weiterreichen von Daten.
	 * 
	 * @param track
	 *            (tracknummer)
	 * @param artist
	 *            (name, nicht id!)
	 * @param album
	 *            (name, nicht id!)
	 * @param title
	 *            (name, nicht id!)
	 * @param genre
	 * @param length
	 *            (in sekunden)
	 * @param p
	 *            (pfad zur datei)
	 */
	public TrackDto(String track, String artist, String album, String title,
			String genre, int length, Path p) {
		super(track, artist, album, title, genre, length, p);
		this.track = track;
		this.title = title;
		this.genre = genre;
		this.length = length;
		this.artistName = artist;
		this.albumName = album;
		path = p;
	}
	

	/**
	 * Konstruktor, wenn die Daten aus der DB kommen.
	 * 
	 * @param title
	 * @param artist
	 * @param album
	 * @param genre
	 * @param track
	 * @param length
	 * @param artistId
	 * @param albumId
	 * @param isChecked
	 * @param path
	 */
//	public TrackDto(int id, String title, String artist, String album,
//			String genre, String track, int length, int artistId, int albumId,
//			boolean isChecked, String path) {
//		this.id = id;
//		this.track = track;
//		this.title = title;
//		this.genre = genre;
//		this.length = length;
//		this.artistName = artist;
//		this.albumName = album;
//		this.artistID = artistId;
//		this.albumID = albumId;
//		this.path = new File(path).toPath();
//		this.checked = isChecked;
//	}

//	public String getStringLength() {
//		String l = "";
//		int h = 0, m = 0, s = 0, tmp = 0;
//		s = length % 60; // sekunden
//		tmp = length - s;
//		m = tmp / 60;// minuten
//		if (m >= 60) {// falls ne stunde erreicht wurde
//			h = m / 60;
//			m = m - h * 60;
//		}
//		if (h != 0) {// falls stunde erreicht
//			l += h + ":";// mit ausgeben
//		}
//		l += h == 0 ? m + ":" : String.format("%02d:", m); // führende nullen
//															// für minuten nur
//															// bei vorhandenen
//															// stunden
//		l += String.format("%02d", s);
//		return l;
//	}

	// #########################>- GETTER & SETTER -<###########################

	public boolean isPlaying() {
		return isPlaying;
	}

	public void setPlaying(boolean isPlaying) {
		this.isPlaying = isPlaying;
	}

	public int getID() {
		return this.id;
	}

	public int getArtistID() {
		return artistID;
	}

	public void setArtistID(int artistID) {
		this.artistID = artistID;
	}

	public int getAlbumID() {
		return albumID;
	}

	public void setAlbumID(int albumID) {
		this.albumID = albumID;
	}

	public String getArtistName() {
		return artistName.equals(null)?"":artistName;
	}

	public void setArtistName(String artistName) {
		this.artistName = artistName;
	}

	public String getAlbumName() {
		return albumName;
	}

	public void setAlbumName(String albumName) {
		this.albumName = albumName;
	}

	public String getTrack() {
		return track;
	}

	public void setTrack(String track) {
		this.track = track;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getGenre() {
		return genre;
	}

	public void setGenre(String genre) {
		this.genre = genre;
	}

	public int getLength() {
		return length;
	}

	public void setLength(int length) {
		this.length = length;
	}

	public Path getPath() {
		return path;
	}

	public void setPath(Path path) {
		this.path = path;
	}

	public boolean isChecked() {
		return checked;
	}

	public void setChecked(boolean checked) {
		// TODO in DB eintragen
		this.checked = checked;
	}

//	public DataFlavor[] getTransferDataFlavors() {
//		// TODO Auto-generated method stub
//		return null;
//	}
//
//	public boolean isDataFlavorSupported(DataFlavor flavor) {
//		// TODO Auto-generated method stub
//		return false;
//	}
//
//	public Object getTransferData(DataFlavor flavor)
//			throws UnsupportedFlavorException, IOException {
//		// TODO Auto-generated method stub
//		return null;
//	}


//
//	/**
//	 * vergleicht den übergebenen track mit this. 
//	 * @param t Track, mit dem this verglichen werden soll
//	 * @param columnConstant Nach welchem Kriterium sortiert werden soll. zu finden in der TrackTableModel Klasse
//	 * @return true, wenn this größer und false, wenn this kleiner oder gleich t ist.
//	 */
//	public boolean compareTo(TrackDto t, char columnConstant) {
//		try {
//			switch (columnConstant) {
//			case TrackTableModel.COL_TITLE:
//				return this.title.compareToIgnoreCase(t.getTitle())>0;
//			case TrackTableModel.COL_ALBUM:
//				return compareAlbum(t);
//			case TrackTableModel.COL_ARTIST:
//				return compareArtist(t);
//			case TrackTableModel.COL_LENGTH:
//				return ((Integer)getLength()).compareTo(t.getLength())>0;
//			case TrackTableModel.COL_GENRE:
//				return getGenre().compareToIgnoreCase(t.getGenre())>0;
//			case TrackTableModel.COL_IS_SELECTED:
//				return isChecked()&&!t.isChecked()?true:false;
	
//		case TrackTableModel.COL_NR:
//			int tr1;
//			int tr2;
//			try {
//				tr1 = Integer.parseInt(getTrack());
//			} catch (NumberFormatException e) {
//				try {
//					tr2 = Integer.parseInt(t.getTrack());
//				} catch (NumberFormatException ev) {
//					return false;
//				}
//				return true;
//			}
//			try {
//				tr2 = Integer.parseInt(t.getTrack());
//			} catch (NumberFormatException e) {
//				return false;
//			}
//			return ((Integer)tr1).compareTo(tr2)>0;
//				
//			default:
//				return false;
//			}
//		} catch (StackOverflowError e) {
//			System.out.println("###ERROR: StackOverflow! Sortier-Rekursion wird hier unterbrochen.###");
//			//e.printStackTrace();
//			return false;
//		}
//	}
//	private boolean compareArtist(TrackDto t) {
//		int temp = this.getArtistName().compareToIgnoreCase(t.getArtistName());
//		if (temp>0) {
//			return true;
//		}else if (temp<0) {
//			return false;
//		}else {// wenn artist gleich, nach album sortieren.
//			return compareAlbum(t);
//		}
//	}
//
//	private boolean compareAlbum(TrackDto t){
//		int temp = this.albumName.compareToIgnoreCase(t.getAlbumName());
//		if (temp==0) {
//			int thisTrack;
//			int tTrack;
//			try {
//				thisTrack = Integer.parseInt(this.track);
//			} catch (NumberFormatException e) {
//				return true;
//			}
//			try {
//				tTrack = Integer.parseInt(t.getTrack());
//			} catch (Exception e) {
//				return false;
//			}
//			return thisTrack > tTrack;
//		}else if(temp>0){
//			return true;
//		}
//		return false;
//	}
}
