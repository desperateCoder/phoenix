package view;

import java.awt.Cursor;
import java.awt.Point;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.dnd.DragSource;
import java.util.List;
import java.util.Map;

import javax.activation.ActivationDataFlavor;
import javax.activation.DataHandler;
import javax.swing.DropMode;
import javax.swing.JComponent;
import javax.swing.JTable;
import javax.swing.JViewport;
import javax.swing.ListSelectionModel;
import javax.swing.TransferHandler;
import javax.swing.event.TableColumnModelEvent;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableColumn;

import model.Library;
import model.Track;
import model.TrackTableModel;
import model.dto.TrackTransferDto;
import view.customs.TableSupport;
import view.customs.TrackTablePopupMenu;
import controller.PhoenixCore;
import controller.interfaces.QuickSearchable;
import controller.interfaces.Reorderable;
import controller.interfaces.customs.FileDropTarget;
import controller.listener.ColumnHeaderListener;
import controller.listener.TableMouseListener;

/**
 * Tabellen-darstellung für die lieder
 * 
 * @author Artur Dawtjan
 * 
 */
@SuppressWarnings("serial")
public class TrackTable extends JTable implements QuickSearchable {
	public static final char PLAYLIST_TYPE = 3;
	public static final char QUEUE_TYPE = 2;
	public static final char LIBRARY_TYPE = 1;
	public TrackTable itself = this;
	PhoenixCore core;
	boolean isCheckboxShown;
	char type;

	public TrackTable(Library lib, Map<Integer, String> colMap, PhoenixCore core) {
		this(lib, colMap, core, true);
	}

	/**
	 * 
	 * @param lib
	 * @param colMap
	 *            map aus
	 * @param core
	 * @param showCheckBox
	 *            checkboxen anzeigen?
	 */
	public TrackTable(Library lib, Map<Integer, String> colMap,	PhoenixCore core, boolean showCheckBox) {
		this(lib, colMap, core, showCheckBox, false);
	}

	/**
	 * 
	 * @param lib
	 * @param colMap
	 *            map aus
	 * @param core
	 * @param showCheckBox
	 *            checkboxen anzeigen?
	 * @param tracksAddable
	 */
	public TrackTable(Library lib, Map<Integer, String> colMap,
			PhoenixCore core, boolean showCheckBox, boolean tracksAddable) {
		this(lib, colMap, core, showCheckBox, tracksAddable, false);
	}

	/**
	 * Konstruktor für Playlist-modus
	 * 
	 * @param core
	 * @param playlistID
	 */
	public TrackTable(Library lib, PhoenixCore core, int playlistID) {
		this(lib, core.getSettings().getTrackTableCols(), core, true, true, true);
	}
	
	/**
	 * Konstruktor, wo alles zusammenläuft.
	 * @param lib Library, die angezeigt werden soll
	 * @param colMap Map aus den Column-Headern
	 * @param core Laufener PhoenixCore
	 * @param showCheckBox sollen Checkboxen angezeigt werden?
	 * @param tracksAddable können tracks per drag n drop hinzugefügt werden?
	 * @param isReorderable kann der user die reihenfolge ändern?
	 */
	public TrackTable(Library lib, Map<Integer, String> colMap, PhoenixCore core, boolean showCheckBox, boolean tracksAddable,	boolean isReorderable) {
		this.core = core;
		this.isCheckboxShown = showCheckBox;
		setModel(new TrackTableModel(lib, colMap, showCheckBox, tracksAddable));
		JTableHeader tableHeader = new JTableHeader(columnModel);
		setTableHeader(tableHeader);
		addMouseListener(new TableMouseListener(core));
		addMouseListener(new TrackTablePopupMenu(this));
		if (showCheckBox) {
			getColumn("").setMaxWidth(0);
		} else {
			clearSortedMarker();
		}
		getColumn("  ").setMaxWidth(25);
		getColumn("Nr.").setMaxWidth(35);
		getColumn("Länge").setMaxWidth(60);
		addKeyListener(core);
		setDragEnabled(true);
		setFillsViewportHeight(true);
		if (!showCheckBox||isReorderable) {// queue-modus
			setDropMode(DropMode.INSERT_ROWS);
			setTransferHandler(new TableRowTransferHandler(this));
			setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
			clearSortedMarker();
			if (isReorderable) {
				this.type = TrackTable.PLAYLIST_TYPE;
			} else if (!showCheckBox) {
				this.type = TrackTable.QUEUE_TYPE;
			}
		} else {// bibliothek
			TableSupport.setQuickSearch(this);
			tableHeader.addMouseListener(new ColumnHeaderListener(this));
			@SuppressWarnings("unused")
			FileDropTarget t = new FileDropTarget(this);
			setTransferHandler(new FromTransferHandler());
			this.type = TrackTable.LIBRARY_TYPE;
		}
	}

	
	@Override
	public boolean isCellEditable(int row, int column) {
		if (isCheckboxShown) {
			return column == 2;// nur checkboxen sind erlaubt
		}
		return false;
	}
	/**
	 * Liefert den Typ dieser TrackTable (playlist? library? queue?)
	 * Entsprechende Konstanten sind in TrackTable enthalten.(XX_TYPE)
	 * @return Typ dieser Table.
	 */
	public char getType() {
		return this.type;
	}
	/**
	 * bekommt mit, wenn per drag n drop eine spalte verschoben wird.
	 */
	@Override
	public void columnMoved(TableColumnModelEvent e) {
		super.columnMoved(e);
	}

	public void setPlaying(Track t, int index) {
		((TrackTableModel) getModel()).setPlaying(t, index);
		repaint();
	}
	public void shuffleTracks() {
		((TrackTableModel) getModel()).shuffleTracks();
	}
	public Track[] getSelectedTracks() {
		return ((TrackTableModel) getModel()).getTracksAt(getSelectedRows());
	}

	/**
	 * Handlet ausgehende drag-n-drop transfers. titel werden uebertragen.
	 * 
	 * @author Artur Dawtjan
	 */
	class FromTransferHandler extends TransferHandler {
		public int getSourceActions(JComponent comp) {
			return COPY_OR_MOVE;
		}

		private int index[];

		// das transferable-objekt bauen
		public Transferable createTransferable(JComponent comp) {
			index = itself.getSelectedRows();
			Track[] tracks = new Track[index.length];// tracks hier reinpacken
			for (int i = 0; i < index.length; i++) {
				TrackTableModel model = ((TrackTableModel) itself.getModel());
				if (index[i] >= 0 || index[i] < model.getRowCount()) {
					tracks[i] = model.getTrackAt(index[i]);
				}
			}

			return new TrackTransferDto(tracks);// und als Transferable-objekt
												// ausspucken.
		}

		/**
		 * wird aufgerufen, wenn export beendet wurde.
		 */
		public void exportDone(JComponent comp, Transferable trans, int action) {
			if (action != MOVE) {
				return;
			}

			// evtl noch etwas tun
		}
	}

	/**
	 * ersetzt die aktuellen Tracks durch die übergebenen
	 * 
	 * @param tracks
	 *            , die hinzugefügt werden sollen
	 */
	public void setTracksInLib(List<Track> tracks) {
		getSelectionModel().clearSelection();
		((TrackTableModel) getModel()).setTracksInLib(tracks);
		((JViewport) getParent()).setViewPosition(new Point(0, 0));
		setFillsViewportHeight(true);
		((JViewport) getParent()).repaint();
	}

	/**
	 * Fuegt die Tracks am Ende der Liste hinzu. vor allem für die Queue-view.
	 * 
	 * @param tracks
	 *            Track[]
	 */
	public void addTracks(Track[] tracks) {
		((TrackTableModel) getModel()).addTracks(tracks);
	}

	/**
	 * triggert das Sortieren der lib
	 * 
	 * @param vColIndex
	 *            Index in der View
	 * @param mColIndex
	 *            Index im Model
	 */
	char lastSortedColumnIndex = TrackTableModel.COL_ARTIST;

	public void sortColumnAt(int vColIndex, int mColIndex) {
		if (mColIndex == 0 || mColIndex == 1 || mColIndex == 2
				|| !isCheckboxShown) {
			return;
		}
		TableColumn col = getColumnModel().getColumn(vColIndex);
		TrackTableModel model = (TrackTableModel) getModel();
		char temp = getColumnConstant(col.getHeaderValue().toString());
		if (lastSortedColumnIndex == temp) {// column bereits angeklickt?
			if (col.getHeaderValue().toString().endsWith(" v")) {// dann den
																	// entsprechenden
																	// suffix
																	// umkehren
				col.setHeaderValue(col
						.getHeaderValue()
						.toString()
						.substring(0,
								col.getHeaderValue().toString().length() - 2)
						+ " ^");
				model.sort(getColumnConstant(col.getHeaderValue().toString()),
						TrackTableModel.SORT_ASC);
			} else if (col.getHeaderValue().toString().endsWith(" ^")) {
				col.setHeaderValue(col
						.getHeaderValue()
						.toString()
						.substring(0,
								col.getHeaderValue().toString().length() - 2)
						+ " v");
				model.sort(getColumnConstant(col.getHeaderValue().toString()),
						TrackTableModel.SORT_DESC);
			}
		} else {
			clearSortedMarker();
			col.setHeaderValue(col.getHeaderValue() + " ^");// neues selektieren
			model.sort(getColumnConstant(col.getHeaderValue().toString()),
					TrackTableModel.SORT_ASC);
		}
		lastSortedColumnIndex = temp;
	}

	private void clearSortedMarker() {
		int count = getColumnModel().getColumnCount();
		String header;
		for (int i = 0; i < count; i++) {// evtl vorher selektierte unselecten
			header = getColumnModel().getColumn(i).getHeaderValue().toString();
			if (header.endsWith(" v") || header.endsWith(" ^")) {
				getColumnModel().getColumn(i).setHeaderValue(
						header.substring(0, header.length() - 2));
			}
		}

	}

	public char getColumnConstant(String s) {
		if (s.startsWith("Titel")) {
			return TrackTableModel.COL_TITLE;
		} else if (s.startsWith("Inter")) {
			return TrackTableModel.COL_ARTIST;
		} else if (s.startsWith("Album")) {
			return TrackTableModel.COL_ALBUM;
		} else if (s.startsWith("L")) {
			return TrackTableModel.COL_LENGTH;
		} else if (s.startsWith("Gen")) {
			return TrackTableModel.COL_GENRE;
		} else if (s.startsWith("Nr")) {
			return TrackTableModel.COL_NR;
		} else {
			return TrackTableModel.COL_ARTIST;
		}
	}

	public void quickSearch(String text) {
		if (isCheckboxShown) {
			((TrackTableModel) getModel()).quickSearch(text);
		}
	}

	/**
	 * Entfernt die momentan ausgewaehlten Tracks aus der Datenbank
	 */
	public void removeSelectedTracks() {
		((TrackTableModel) getModel()).removeTracksAt(getSelectedRows());
	}
}

/**
 * Handles drag & drop row reordering
 */
@SuppressWarnings("serial")
class TableRowTransferHandler extends TransferHandler {
	private final DataFlavor localObjectFlavor = new ActivationDataFlavor(
			Integer.class, DataFlavor.javaJVMLocalObjectMimeType,
			"Integer Row Index");
	private JTable table = null;

	public TableRowTransferHandler(JTable table) {
		this.table = table;
	}

	@Override
	protected Transferable createTransferable(JComponent c) {
		assert (c == table);
		return new DataHandler(new Integer(table.getSelectedRow()),
				localObjectFlavor.getMimeType());
	}

	@Override
	public boolean canImport(TransferHandler.TransferSupport info) {
		boolean b = info.getComponent() == table && info.isDrop()
				&& info.isDataFlavorSupported(localObjectFlavor);
		table.setCursor(b ? DragSource.DefaultMoveDrop
				: DragSource.DefaultMoveNoDrop);
		return b;
	}

	@Override
	public int getSourceActions(JComponent c) {
		return TransferHandler.COPY_OR_MOVE;
	}

	@Override
	public boolean importData(TransferHandler.TransferSupport info) {
		JTable target = (JTable) info.getComponent();
		JTable.DropLocation dl = (JTable.DropLocation) info.getDropLocation();
		int index = dl.getRow();
		int max = table.getModel().getRowCount();
		if (index < 0 || index > max)
			index = max;
		target.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
		try {
			Integer rowFrom = (Integer) info.getTransferable().getTransferData(
					localObjectFlavor);
			if (rowFrom != -1 && rowFrom != index) {
				((Reorderable) table.getModel()).reorder(rowFrom, index);
				if (index > rowFrom)
					index--;
				target.getSelectionModel().addSelectionInterval(index, index);
				return true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	@Override
	protected void exportDone(JComponent c, Transferable t, int act) {
		if (act == TransferHandler.MOVE) {
			table.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
		}
	}
}