package view;

import java.awt.Point;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;

import javax.swing.BoundedRangeModel;
import javax.swing.JSlider;

import model.constants.SettingKeys;
import controller.PhoenixCore;

@SuppressWarnings("serial")
public class ClickSlider extends JSlider{
	private Display display = null;
	public ClickSlider() {
		super();
		setPaintTicks(false);
		addMouseListener();
	}
	public ClickSlider(BoundedRangeModel m){
		super(m);
		addMouseListener();

	}
	public ClickSlider(int i, int j, int k) {
		super(i,j,k);
		addMouseListener();

	}
	
	public ClickSlider(BoundedRangeModel pipe, Display display) {
		this(pipe);
		this.display = display;
	}
	
	private void addMouseListener() {
//		MouseListener[] ml = getMouseListeners();
//		for (MouseListener mli : ml) {
//			removeMouseListener(mli);
//		}
		addMouseWheelListener(new MouseWheelListener() {
			private int vol;
			private int waitTime = 0;
			private Runnable adjustThread = new Runnable() {
				
				@Override
				public void run() {
					while (waitTime > 0) {
						waitTime -= 100;
						try {
							Thread.sleep(100);
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
					}
//					System.out.println("vol set to "+vol);
					PhoenixCore.INSTANCE.getSettings().setSetting(SettingKeys.PLAYER_VOLUME, vol+"");
				}
			};
			@Override
			public void mouseWheelMoved(MouseWheelEvent e) {
				int amount = e.getWheelRotation();
				vol = getModel().getValue()+(-3*amount);
				
				if (vol<0) 
				vol = 0;
				else if (vol > 100)
				vol=100;
				getModel().setValue(vol);
				if (waitTime < 1) {
					waitTime = 1000;
					new Thread(adjustThread).start();
//					System.out.println("thread started");
				}else {
					waitTime = 1000;
				}
				
			}
		});
		addMouseListener(new MouseAdapter() {
			
			
	         @Override
	         public void mouseClicked(MouseEvent e) {
	        	if (getValueIsAdjusting()) {
					return;
				}
//	        	System.out.println("clicked");
				int val = getValueForEvent(e);
				getModel().setValue(val);
				PhoenixCore.INSTANCE.getSettings().setSetting(SettingKeys.PLAYER_VOLUME, val+"");
	         }
	      });
	}
	
	private int getValueForEvent(MouseEvent e){
		Point p = e.getPoint();
        double percent = p.x / ((double) getWidth());
        int range = getMaximum() - getMinimum();
        double newVal = range * percent;
        return (int)(getMinimum() + newVal);
	}
	
	@Override
	public void repaint() {
		int n = getValue();
		int rem = getMaximum()-n;
		
		try {
			display.setLengthString(n, rem);
		} catch (Exception e) {
			//e.printStackTrace();
		}
		super.repaint();
	}
	
	public void setValueTo(int val){
		setValue(val);
		PhoenixCore.INSTANCE.getSettings().setSetting(SettingKeys.PLAYER_VOLUME, val+"");
	}
}
