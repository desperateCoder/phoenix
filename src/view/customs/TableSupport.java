package view.customs;
import java.awt.BorderLayout;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import controller.interfaces.QuickSearchable;

/**
 * Diese klasse unterstützt eine jtable(DIE QuickSearchable IMPLEMENTIERT!!) mit einer quicksearch-funktion.
 * die wegen dem interface zu implementierende methode handlet den quicksearch.
 * @author Artur Dawtjan
 * 
 */
public class TableSupport {
	/**
	 * stattet die übergebene JTable mit quicksearch-fähigkeit aus.
	 * @param table, die ausgestattet werden soll.
	 */
	public static void setQuickSearch(final JTable table) {
		setQuickSearch(table, 3);
	}
	/**
	 * stattet die übergebene JTable mit quicksearch-fähigkeit aus.
	 * @param table, die ausgestattet werden soll.
	 * @param startLenght, ab welchem zeichen soll gesucht werden?
	 */
    public static void setQuickSearch(final JTable table, int startLength) {
    	final int start = startLength-1;
        final JTextField searchField = new JTextField();
        
        /**
         * Hilfsklasse, die den quicksearch einleitet.
         * @author artur
         *
         */
        class Search {
        	/**
        	 * leitet die suche ein, sobald ein mindestens dreistelliger suchbegriff eingegeben wurde
        	 */
            void search() {
                String text = searchField.getText();
                if (text.length()>=start) {
					((QuickSearchable)table).quickSearch(text);
				}
            }
        }
        table.addKeyListener(new KeyAdapter() {
        	boolean isOpen =false;
        	
            @Override

            public void keyPressed(final KeyEvent evt) {
            	//aufruf bei eingegebenem suchbegriff
                char ch = evt.getKeyChar();
                //schon ein fenster offen?
                if (isOpen) {
                	//dann buchstaben einfach anhängen
                	searchField.setText(searchField.getText()+ch);
					return;
				}
                if (!Character.isLetterOrDigit(ch)) {//nur wenn eingabe eine zahl oder ein buchstabe ist...
                    return;// ... darf fortgefahren werden, sonst raus.
                }
                isOpen = true;
                int selectedRow = table.getSelectedRow();
                int selectedColumn = table.getSelectedColumn();
                Object clientProperty = table.getClientProperty("JTable.autoStartsEdit");
                if ((clientProperty == null || (Boolean) clientProperty)
                        && selectedRow >= 0 && selectedColumn >= 0
                        && table.isCellEditable(table.getSelectedRow(), table.getSelectedColumn())) {
                    return;
                }
                //suche einleiten
                final Search s = new Search();
                s.search();
                final JDialog d = new JDialog();
                d.setUndecorated(true);
                d.setSize(200, 30);
                Point p = table.getTableHeader().getLocationOnScreen();
                d.setLocation(p);
                final JLabel lb = new JLabel("Suche: ");
                d.add(lb, BorderLayout.LINE_START);
                d.add(searchField);
                searchField.setText(String.valueOf(ch));
                d.setVisible(true);
                searchField.getDocument().addDocumentListener(new DocumentListener() {
 
                    
                    public void insertUpdate(final DocumentEvent e) {
                        s.search();
                    }
 
                    
                    public void removeUpdate(final DocumentEvent e) {
                        s.search();
                    }
 
                    
                    public void changedUpdate(final DocumentEvent e) {
                        s.search();
                    }
                });
                searchField.addFocusListener(new FocusListener() {
 
                    
                    public void focusGained(final FocusEvent e) {
                    }
 
                    
                    public void focusLost(final FocusEvent e) {
                    	isOpen = false;
                        d.dispose();
                    }
                });
                @SuppressWarnings("serial")
				Action exit = new AbstractAction() {
 
                    
                    public void actionPerformed(final ActionEvent e) {
                        d.dispose();
                        isOpen = false;
                    }
                };
                searchField.setAction(exit);
                searchField.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(
                        KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), "exit");
                searchField.getActionMap().put("exit", exit);
            }
        });
    }
}