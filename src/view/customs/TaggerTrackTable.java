package view.customs;

import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import view.views.TaggerView;

import model.Library;
import model.TaggerTrackTableModel;
import model.Track;
import controller.PhoenixCore;
import controller.listener.TableMouseListener;

@SuppressWarnings("serial")
public class TaggerTrackTable extends JTable {
	PhoenixCore core;
	TaggerView view;
	public TaggerTrackTable(TaggerView view, PhoenixCore core) {
		super();
		this.view = view;
		this.core = core;
		setModel(new TaggerTrackTableModel());
		addMouseListener(new TableMouseListener(core));
		getSelectionModel().addListSelectionListener(new SelectionListener(this));
		getSelectionModel().setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		getColumn("S").setMaxWidth(25);
	}

	public void addTracks(Track[] tracks) {
		((TaggerTrackTableModel)getModel()).addTracks(tracks);
	}

	public void setPlaying(Track t, int index) {
		// evtl symbol ändern
		
	}

	public Library getLibrary() {
		return ((TaggerTrackTableModel)getModel()).getLib();
		
	}

	public void setSelectedRows() {
		view.setSelectedRows(((TaggerTrackTableModel)getModel()).getSelectedTracks(getSelectedRows()), getSelectedRows());
	}
	public void writeTags(){
		((TaggerTrackTableModel)getModel()).writeTags();
	}
}
class SelectionListener implements ListSelectionListener {
    TaggerTrackTable table;

    // It is necessary to keep the table since it is not possible
    // to determine the table from the event's source
    SelectionListener(TaggerTrackTable table) {
        this.table = table;
    }
    public void valueChanged(ListSelectionEvent e) {
        if (!e.getValueIsAdjusting()) {
                table.setSelectedRows();
        }
    }
}
