package view.customs;

import java.awt.Rectangle;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JTable;

import controller.interfaces.QuickSearchable;

@SuppressWarnings("serial")
public class ScrollByClickOnHeaderTable extends JTable implements
		MouseListener, QuickSearchable {
	public ScrollByClickOnHeaderTable(Object[][] art, String[] strings, MouseListener listenForDoubleCliks) {
		super(art, strings);
		getTableHeader().addMouseListener(this);
		addMouseListener(listenForDoubleCliks);
	}

	// methode überschreiben, damit zellen nicht mehr editiert werden können.
	@Override
	public boolean isCellEditable(int row, int column) {
		return false;
	}

	public void quickSearch(String text) {
		// table.clearSelection();
		if (text.length() == 0) {
			return;
		}
		for (int row = 0; row < getRowCount(); row++) {
			Object val = getValueAt(row, 0);
			String value = val != null ? val.toString() : "";
			if (value.toLowerCase().startsWith(text.toLowerCase())) {
				changeSelection(row, 0, false, false);
				break;
			}
		}
	}

	// ----------------mouse-Listener für interpreten- und
	// album-table---------------------
	public void mouseClicked(MouseEvent e) {
		getSelectionModel().setSelectionInterval(0, 0);
		scrollRectToVisible(new Rectangle(0, 0));
	}

	public void mousePressed(MouseEvent e) {
	}

	public void mouseReleased(MouseEvent e) {
	}

	public void mouseEntered(MouseEvent e) {
	}

	public void mouseExited(MouseEvent e) {
	}
}
