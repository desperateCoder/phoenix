package view.customs;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;

import controller.PhoenixCore;

import view.TrackTable;

@SuppressWarnings("serial")
public class TrackTablePopupMenu extends JPopupMenu implements ActionListener, MouseListener, FocusListener{
	
	private final static String ITEM_REMOVE_FROM_PLAYLIST="a";
	private final static String ITEM_DELETE_TRACK_FILE="b";
	private final static String ITEM_ADD_TO_TAGGER="c";
	private final static String ITEM_RANDOMIZE_QUEUE = "d";
	TrackTable table;
	List<JMenuItem> itemsToShow=new ArrayList<JMenuItem>();
	JMenuItem removeTracks = null;
	JMenuItem deleteTracks=null;
	JMenuItem addToTagger=null;
	JMenuItem randomizeQueue=null;
	
	public TrackTablePopupMenu(TrackTable table) {
		this.table = table;
		this.addFocusListener(this);
		removeTracks = new JMenuItem("Auswahl aus der Liste entfernen");
		removeTracks.addActionListener(this);
		removeTracks.setActionCommand(TrackTablePopupMenu.ITEM_REMOVE_FROM_PLAYLIST);

		deleteTracks = new JMenuItem("Auswahl von der Festplatte löschen");
		deleteTracks.addActionListener(this);
		deleteTracks.setActionCommand(TrackTablePopupMenu.ITEM_DELETE_TRACK_FILE);
		
		addToTagger = new JMenuItem("Auswahl zum Tagger hinzufügen");
		addToTagger.addActionListener(this);
		addToTagger.setActionCommand(TrackTablePopupMenu.ITEM_ADD_TO_TAGGER);
		
		randomizeQueue = new JMenuItem("Warteschlange mischen");
		randomizeQueue.addActionListener(this);
		randomizeQueue.setActionCommand(TrackTablePopupMenu.ITEM_RANDOMIZE_QUEUE );
	}
	public void actionPerformed(ActionEvent e) {
		if (e.getActionCommand().equals(TrackTablePopupMenu.ITEM_REMOVE_FROM_PLAYLIST)) {
			this.table.removeSelectedTracks();
			this.table.validate();
			this.table.repaint();
		}else if (e.getActionCommand().equals(TrackTablePopupMenu.ITEM_DELETE_TRACK_FILE)){
			int sel = this.table.getSelectedTracks().length;
			String txt = sel>1?"Sollen die ausgewählten Dateien ("+sel+")":"Soll die ausgewählte Datei";
			int yn = JOptionPane.showConfirmDialog(PhoenixCore.INSTANCE.getCurrentGUI(), txt+" wirklich\nvon der Festplatte gelöscht werden?", "Sicher?", JOptionPane.YES_NO_OPTION);
			if (yn==0) {
				JOptionPane.showMessageDialog(this.table, "und weg");
//				this.table.removeSelectedTracks();
//				this.table.validate();
//				this.table.repaint();
			}else {
				JOptionPane.showMessageDialog(this.table, "noch da!");
			}
			
		}else if (e.getActionCommand().equals(TrackTablePopupMenu.ITEM_ADD_TO_TAGGER)){
			
		}else if (e.getActionCommand().equals(TrackTablePopupMenu.ITEM_RANDOMIZE_QUEUE)){
			table.shuffleTracks();
		}
	}
	@Override
	public void setVisible(boolean b) {
		if (!b) {
			super.setVisible(b);
		}
		removeAll();
		switch (table.getType()) {
		case TrackTable.LIBRARY_TYPE:
			if (this.table.getSelectedTracks().length>0) {
				add(addToTagger);
				add(deleteTracks);
			}
			break;

		case TrackTable.PLAYLIST_TYPE:
			add(addToTagger);
			if (this.table.getSelectedTracks().length>0) {
				add(removeTracks);
				add(deleteTracks);
			}
			break;
			
		case TrackTable.QUEUE_TYPE:
			if (this.table.getSelectedTracks().length>0) {
				add(addToTagger);
				addSeparator();
				add(removeTracks);
				add(deleteTracks);
				addSeparator();
			}
			if (this.table.getModel().getRowCount()>0) {
				add(randomizeQueue);
			}
				
			break;
		}
		super.setVisible(b);
	}
	public void mouseClicked(MouseEvent e) {
		if (e.getButton()==MouseEvent.BUTTON3) {
			super.show(table, e.getX(), e.getY());
		}
	}
	public void mousePressed(MouseEvent e) {}
	public void mouseReleased(MouseEvent e) {}
	public void mouseEntered(MouseEvent e) {}
	public void mouseExited(MouseEvent e) {}
	public void focusGained(FocusEvent e) {}
	public void focusLost(FocusEvent e) {
		setVisible(false);
	}
}
