package view.customs;

import javax.swing.tree.DefaultMutableTreeNode;
/**
 * TreeNode, welcher als drag-n-drop geeignet durchgeht. oder auch nicht. 
 * gesteuert über das boolean-flag isDropEnabled
 * 
 * @author Artur Dawtjan
 */
@SuppressWarnings("serial")
public class DropTreeNode extends DefaultMutableTreeNode {
	private boolean isDropEnabled;
	
	public DropTreeNode(String title) {
		this(title, false);
	}
	public DropTreeNode(Object userObject) {
		this(userObject, false);
	}
	public DropTreeNode(String title, boolean enableDrop) {
		super(title);
		isDropEnabled = enableDrop;
	}
	public DropTreeNode(Object userObject, boolean enableDrop) {
		super(userObject);
		isDropEnabled = enableDrop;
	}
	/**
	 * Sagt aus, ob dieser knoten zum Drop taugt.
	 * @return
	 */
	public boolean canBeDropTarget(){
		return isDropEnabled;
	}
	@Override
	public String toString() {
		return getUserObject().toString();
	}
}
