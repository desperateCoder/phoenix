package view.customs;

import java.awt.Dimension;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

import javax.swing.JTextField;

@SuppressWarnings("serial")
public class TaggerTextField extends JTextField implements FocusListener{
	TaggerEditForm listener;
	public TaggerTextField(TaggerEditForm l) {
		super();
		listener = l;
		setPreferredSize(new Dimension(200, 27));
		addFocusListener(this);
	}
	//FocusListener
	public void focusGained(FocusEvent e) {
		if (isEditable()) {
			selectAll();
		}
	}
	public void focusLost(FocusEvent e) {
		select(0, 0);
	}
	
}
