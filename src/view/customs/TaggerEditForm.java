package view.customs;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import model.TaggerTrackTableModel;
import model.Track;
import controller.listener.TagChangeListener;
/**
 * in dieses formular werden die tags eingetragen, die in die datei geschrieben werden sollen
 * @author Artur Dawtjan
 *
 */
@SuppressWarnings("serial")
public class TaggerEditForm extends JPanel implements ActionListener {
	List<Track> selectedTracks;
	int[] selection;
	
	TagChangeListener listener;
	//formularkomponenten
	JLabel headLabel;
	JButton apply;
	TaggerTextField fileNameField;
	TaggerTextField titleField;
	TaggerTextField albumField;
	TaggerTextField artistField;
	TaggerTextField genreField;
	TaggerTextField trackField;
	JTextField pathField;
	
	public TaggerEditForm(TaggerTrackTableModel tableModel) {
		super();
		selectedTracks = new ArrayList<Track>();
		listener = tableModel;
		//formularfelder initialisieren
		fileNameField = new TaggerTextField(this);
		Dimension d = new Dimension(2200,30);
		fileNameField.setMaximumSize(d);
		titleField = new TaggerTextField(this);
		titleField.setMaximumSize(d);
		albumField = new TaggerTextField(this);
		albumField.setMaximumSize(d);
		artistField = new TaggerTextField(this);
		artistField.setMaximumSize(d);
		trackField = new TaggerTextField(this);
		trackField.setMaximumSize(d);
		genreField = new TaggerTextField(this);
		genreField.setMaximumSize(d);
		pathField=new JTextField();
		pathField.setMaximumSize(d);
		pathField.setEditable(false);
		
		//Panel zusammenbasteln
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		headLabel = new JLabel("Tags Bearbeiten");
		headLabel.setFont(new Font("Verdana", Font.BOLD, 16));
		add(headLabel);
		//	formular-panel
		JPanel formBox = new JPanel();
		formBox.setLayout(new BorderLayout());
		JPanel leftBox = new JPanel();
		leftBox.setLayout(new BoxLayout(leftBox, BoxLayout.Y_AXIS));
		JPanel rightBox = new JPanel();
		rightBox.setLayout(new BoxLayout(rightBox, BoxLayout.Y_AXIS));
		
		JLabel fileNameLabel = new JLabel("Dateiname:");
		JLabel titleLabel = new JLabel("Titel:");
		JLabel albumLabel = new JLabel("Album:");
		JLabel artistLabel = new JLabel("Interpret:");
		JLabel genreLabel = new JLabel("Genre:");
		JLabel trackLabel = new JLabel("Track-Nr.:");
		
		Font f = new Font("Verdana", Font.BOLD, 14);
		fileNameLabel.setFont(f);
		titleLabel.setFont(f);
		albumLabel.setFont(f);
		artistLabel.setFont(f);
		trackLabel.setFont(f);
		genreLabel.setFont(f);
		
		
		rightBox.add(fileNameField);
		rightBox.add(trackField);
		rightBox.add(titleField);
		rightBox.add(albumField);
		rightBox.add(artistField);
		rightBox.add(genreField);
		rightBox.add(pathField);

		Dimension space = new Dimension(0, 12);
		leftBox.add(Box.createRigidArea(new Dimension(0,3)));
		leftBox.add(fileNameLabel);
		leftBox.add(Box.createRigidArea(space));
		leftBox.add(trackLabel);
		leftBox.add(Box.createRigidArea(space));
		leftBox.add(titleLabel);
		leftBox.add(Box.createRigidArea(space));
		leftBox.add(albumLabel);
		leftBox.add(Box.createRigidArea(space));
		leftBox.add(artistLabel);
		leftBox.add(Box.createRigidArea(space));
		leftBox.add(genreLabel);
		
		formBox.add(leftBox, BorderLayout.WEST);
		formBox.add(rightBox, BorderLayout.CENTER);
		apply = new JButton("Übernehmen");
		apply.addActionListener(this);
		formBox.add(apply, BorderLayout.SOUTH);
		add(formBox);
		listener = tableModel;
	}
	public void setSelectedTracks(Track[] selectedTracks, int[] sel){
		this.selectedTracks.clear();
		selection = sel;
		for (int i = 0; i < selectedTracks.length; i++) {
			this.selectedTracks.add(selectedTracks[i]);
		}
		fillForm();
	}
	public void clearForm(){
		fileNameField.setText("");
		titleField.setText("");
		albumField.setText("");
		artistField.setText("");
		trackField.setText("");
		genreField.setText("");
		pathField.setText("");
	}
	/**
	 * füllt das Formular mit den aktuellen Daten.
	 */
	private void fillForm() {
		//nur eine Datei?
		if (selectedTracks.size()==1) {//dann einfach alles anzeigen
			Track t = selectedTracks.get(0);
			fileNameField.setText(t.getPath().getFileName().toString());
			titleField.setText(t.getTitle());
			albumField.setText(t.getAlbumName());
			artistField.setText(t.getArtistName());
			trackField.setText(t.getTrack());
			genreField.setText(t.getGenre());
			fileNameField.setEditable(true);
			fileNameField.setEnabled(true);
			pathField.setText(t.getPath().toString());
		}else if(selectedTracks.size()>0){//ansonsten sinds mehrere Tracks
			//es wird überprüft, welche merkmale dieser Lieder gleich sind.
			fileNameField.setEditable(false);
			fileNameField.setEnabled(false);
			String title = " ";
			String album = " ";
			String artist = " ";
			String track = " ";
			String genre = " ";
			String fileName = "";
			Track selTrack;
			Track temp = selectedTracks.get(0);
			for (int i = 1; i < selectedTracks.size(); i++) {
				//tracks miteinander vergleichen
				selTrack = selectedTracks.get(i);
				// noch keine unterschiede bisher und auch aktuell nicht? dann übernehmen, sonst mit != markieren
				title = !title.equals("!=")&&selTrack.getTitle().equals(temp.getTitle())?selTrack.getTitle():"!=";
				album = !album.equals("!=")&&selTrack.getAlbumName().equals(temp.getAlbumName())?selTrack.getAlbumName():"!=";
				artist = !artist.equals("!=")&&selTrack.getArtistName().equals(temp.getArtistName())?selTrack.getArtistName():"!=";
				track = !track.equals("!=")&&selTrack.getTrack().equals(temp.getTrack())?selTrack.getTrack():"!=";
				genre = !genre.equals("!=")&&selTrack.getGenre().equals(temp.getGenre())?selTrack.getGenre():"!=";
				fileName = !fileName.equals("!=")&&selTrack.getPath().getFileName().toString().equals(temp.getPath().getFileName()
						.toString())?selTrack.getPath().getFileName().toString():"!=";
			}

			fileNameField.setText(fileName);
			titleField.setText(title);
			albumField.setText(album);
			artistField.setText(artist);
			trackField.setText(track);
			genreField.setText(genre);
			pathField.setText("!=");
		}
		
	}
	public void actionPerformed(ActionEvent e) {
		for (Track t : selectedTracks) {
			if(!trackField.getText().equals("!=")){
				t.setTrack(trackField.getText());
			}
			if(!titleField.getText().equals("!=")){
				t.setTitle(titleField.getText());
			}
			if(!albumField.getText().equals("!=")){
				t.setAlbumName(albumField.getText());
			}
			if(!artistField.getText().equals("!=")){
				t.setArtistName(artistField.getText());
			}
			if(!genreField.getText().equals("!=")){
				t.setGenre(genreField.getText());
			}
		}
		listener.tagsChanged(selectedTracks, selection);
		clearForm();
	}
	
}
