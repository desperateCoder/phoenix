package view.customs;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.tree.DefaultMutableTreeNode;

import view.LibTreeView;
import view.views.PlaylistView;

@SuppressWarnings("serial")
public class TreePopupMenu extends JPopupMenu implements ActionListener{
	private final static String ITEM_NEW_PLAYLIST="a";
	private final static String ITEM_DELETE_PLAYLIST="b";
	private final static String ITEM_RENAME_PLAYLIST="c";
	LibTreeView tree;
	List<JMenuItem> itemsToShow=new ArrayList<JMenuItem>();
	JMenuItem newPlaylist = null;
	JMenuItem deletePlaylist=null;
	JMenuItem renamePlaylist=null;
	public TreePopupMenu(LibTreeView tree) {
		this.tree = tree;
		newPlaylist = new JMenuItem("Neue Wiedergabeliste...");
		newPlaylist.addActionListener(this);
		newPlaylist.setActionCommand(TreePopupMenu.ITEM_NEW_PLAYLIST);

		deletePlaylist = new JMenuItem("Löschen");
		deletePlaylist.addActionListener(this);
		deletePlaylist.setActionCommand(TreePopupMenu.ITEM_DELETE_PLAYLIST);
		
		renamePlaylist = new JMenuItem("Umbenennen");
		renamePlaylist.addActionListener(this);
		renamePlaylist.setActionCommand(TreePopupMenu.ITEM_RENAME_PLAYLIST);
	}
	public void actionPerformed(ActionEvent e) {
		if (e.getActionCommand().equals(TreePopupMenu.ITEM_NEW_PLAYLIST)) {
			tree.addPlaylist();
		}else if (e.getActionCommand().equals(TreePopupMenu.ITEM_DELETE_PLAYLIST)){
			tree.deletePlaylist();
		}else if (e.getActionCommand().equals(TreePopupMenu.ITEM_RENAME_PLAYLIST)){
			tree.renamePlaylist();
		}
	}
	@Override
	public void setVisible(boolean b) {
		if (b) {
			itemsToShow.clear();
			removeAll();
			boolean isPlaylistSelected=false;
			DefaultMutableTreeNode node=null;
			try {
				node =((DefaultMutableTreeNode)tree.getLastSelectedPathComponent());
				isPlaylistSelected=node.getUserObject().toString().equals("Wiedergabelisten");
			} catch (Exception e) {
				return;
			}
			if (isPlaylistSelected) {//der playlist-parent-node wurde selektiert.
				itemsToShow.add(newPlaylist);
			}
			else if (node.getUserObject() instanceof PlaylistView) {//eine playlist wurde selektiert
				itemsToShow.add(renamePlaylist);
				itemsToShow.add(deletePlaylist);
			}
			for (int i = 0; i < itemsToShow.size(); i++) {
				add(itemsToShow.get(i));	
			}
			if (itemsToShow.size()>0) {
				super.setVisible(true);
			}
		}else {
			super.setVisible(false);
		}
	}
}
