package view;

import java.awt.BorderLayout;

import javax.swing.JPanel;

import model.Library;
import model.Track;
/**
 * Abstrakte klasse für eine view. die views sind das, was ins mittlere panel im splitpane gelegt wird.
 * beinhaltet die nötigsten implementierungen für die view. 
 * @author Artur Dawtjan
 *
 */
@SuppressWarnings("serial")
public abstract class AbstractView extends JPanel {
	private String name;
	protected Library lib;

	public AbstractView(String name) {
		this.name = name;
		setLayout(new BorderLayout());
	}
	
	/**
	 * liefert die bibliothek dieser view
	 * @return Library: Daten dieser view.
	 */
	public Library getLibrary(){
		return lib;
	}
	@Override
	public String toString() {
		return this.name;
	}
	
	/**
	 * wird aufgerufen, wenn das lied wechselt. 
	 * soll das in der tracktable anzeigen.
	 * @param t 
	 * @param index
	 */
	public abstract void setPlaying(Track t, int index);
	
	/**
	 * Tracks zur view und ggf in die DB eintragen
	 * @param tracks
	 * @param dbCon
	 */
	public abstract void addTracks(Track[] tracks);
	
	
}

