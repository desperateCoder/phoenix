package view;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;

import controller.PhoenixCore;
/**
 * Stellt Prozesse dar, die im hintergrund laufen
 * @author Artur Dawtjan
 *
 */
@SuppressWarnings("serial")
public class ProgressPane extends JPanel {
	JProgressBar progress;
	JLabel label;
	PhoenixCore core;
	public ProgressPane(PhoenixCore core) {
		this.core = core;
		progress = new JProgressBar();
	}
}
