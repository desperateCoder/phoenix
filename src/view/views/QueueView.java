package view.views;

import javax.swing.JScrollPane;

import model.Library;
import model.Track;
import view.AbstractView;
import view.TrackTable;
import controller.PhoenixCore;

@SuppressWarnings("serial")
public class QueueView extends AbstractView {
	TrackTable trackTable;
	public QueueView(String name, PhoenixCore core) {
		super(name);
		lib = new Library(false);
		trackTable = new TrackTable(lib, core.getSettings().getTrackTableCols(false), core, false, true);
		add(new JScrollPane(trackTable, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER));
//		setTransferHandler(new ToTransferHandler(1));
	}

	@Override
	public void setPlaying(Track t, int index) {
		trackTable.setPlaying(t, index);
	}

	@Override
	public void addTracks(Track[] tracks) {
		trackTable.addTracks(tracks);
	}
	public TrackTable getTrackTable(){
		return trackTable;
	}
	
}
