package view.views;

import java.awt.BorderLayout;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTable;
import javax.swing.JViewport;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableModel;

import model.Album;
import model.Artist;
import model.Library;
import model.Track;
import model.constants.ActionCommands;
import model.constants.SettingKeys;
import view.AbstractView;
import view.TrackTable;
import view.customs.ScrollByClickOnHeaderTable;
import view.customs.TableSupport;
import controller.PhoenixCore;
import controller.listener.AlbumSelectionListener;
import controller.listener.ArtistSelectionListener;
import controller.listener.DividerLocationPropertyChangeListener;

/**
 * das panel der bibliothek.
 * @author Artur Dawtjan
 *
 */
@SuppressWarnings("serial")
public class LibView extends AbstractView{
	TrackTable trackTable;
	JSplitPane sortSplit;
	JSplitPane mainSplit;
	JTable artists;
	JTable albums;
	public LibView(String name, PhoenixCore core) {
		super(name);
		mainSplit = new JSplitPane(JSplitPane.VERTICAL_SPLIT);
		
		sortSplit = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT);
		
		sortSplit.setResizeWeight(0.5);
		lib = new Library(true);
		MouseListener ml = new MouseListener() {
			@Override
			public void mouseReleased(MouseEvent e) {}
			
			@Override
			public void mousePressed(MouseEvent e) {}
			
			@Override
			public void mouseExited(MouseEvent e) {}
			
			@Override
			public void mouseEntered(MouseEvent e) {}
			
			@Override
			public void mouseClicked(MouseEvent e) {
				if (e.getButton()==MouseEvent.BUTTON1 && e.getClickCount()>1) {
					PhoenixCore.INSTANCE.actionPerformed(new ActionEvent(this, ActionCommands.PLAY_LIB, ActionCommands.PLAY));
				}
			}
		};
		
		//übertragen der Artists in table-gerechtes array
		int length=lib.getArtists().length;
		Artist[][] art = new Artist[length+1][length+1];
		art[0][0] = new Artist(0, "Alle ("+length+")"); 	//<-- Die 0 als ID signalisiert, dass Alle angezeigt werden sollen
		for (int i = 1; i <= length; i++) {
			art[i][0] = lib.getArtists()[i-1];// i-1 weil alle noch oben drüber steht und i bei eins beginnt.
		}
		artists = new ScrollByClickOnHeaderTable(art, new String[]{"Interpreten"}, ml);
		
		//übertragen der Albums in table-gerechtes array
		length=lib.getAlbums().length;
		Album[][] alb = new Album[length+1][length+1];
		alb[0][0] = new Album(0, "Alle ("+length+")");		//<-- Die 0 als ID signalisiert, dass Alle angezeigt werden sollen
		for (int i = 1; i <= length; i++) {
			alb[i][0] = lib.getAlbums()[i-1];// i-1 weil alle noch oben drüber steht und i bei eins beginnt.
		}
		albums = new ScrollByClickOnHeaderTable(alb, new String[]{"Alben"}, ml);
		
		
		artists.setFillsViewportHeight(true);
		albums.setFillsViewportHeight(true);
		TableSupport.setQuickSearch(albums,1);
		TableSupport.setQuickSearch(artists,1);
		
		sortSplit.add(new JScrollPane(artists, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER));
		sortSplit.add(new JScrollPane(albums, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER));
		mainSplit.add(sortSplit);
		trackTable = new TrackTable(lib, core.getSettings().getTrackTableCols(), core);
		mainSplit.add(new JScrollPane(trackTable));
		add(mainSplit, BorderLayout.CENTER);
		mainSplit.setResizeWeight(0.3);
		
		artists.getSelectionModel().setSelectionInterval(0, 0);
		artists.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		artists.getSelectionModel().addListSelectionListener(new ArtistSelectionListener(artists, this));
		albums.getSelectionModel().setSelectionInterval(0, 0);
		albums.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		albums.getSelectionModel().addListSelectionListener(new AlbumSelectionListener(albums, this));
		
		mainSplit.setDividerLocation(PhoenixCore.INSTANCE.getSettings().getSettingAsInt(SettingKeys.SPLIT_MAIN_LIBVIEW_POS));
		mainSplit.addPropertyChangeListener(
				JSplitPane.DIVIDER_LOCATION_PROPERTY,
				new DividerLocationPropertyChangeListener(SettingKeys.SPLIT_MAIN_LIBVIEW_POS, mainSplit));
		
		sortSplit.setDividerLocation(PhoenixCore.INSTANCE.getSettings().getSettingAsInt(SettingKeys.SPLIT_ALBUM_LIBVIEW_POS));
		sortSplit.addPropertyChangeListener(
				JSplitPane.DIVIDER_LOCATION_PROPERTY,
				new DividerLocationPropertyChangeListener(SettingKeys.SPLIT_ALBUM_LIBVIEW_POS, sortSplit));
		
	}
	
	@Override
	public void setPlaying(Track t, int index) {
		trackTable.setPlaying(t, index);
	}

	@Override
	public void addTracks(Track[] tracks) {
		for (int i = 0; i < tracks.length; i++) {
			System.out.println("Titel: " + tracks[i].getTitle());
			//TODO hier das hinzufuegen handlen
		}
	}
	
	/**
	 * Diese methode wird vom ArtistSelectionListener aufgerufen, wenn ein artist selektiert wird
	 * @param selectedArtist Artist, der selektiert wurde
	 */
	public void artistSelected(Artist selectedArtist){
		ResultSet rsTemp;
		Album[][] alb;
		List<Album> alben= new ArrayList<Album>();
		int length=0;
		if (selectedArtist.getId()==0) {//"alle" wurde selektiert.
			rsTemp = PhoenixCore.DBCON.executeOnLib("SELECT al.id, al.name from albums as al " + //also alle aleben holen
					"inner join artists as a on a.id = al.artist " +
					"order by lower(al.name) asc");
			try {
				
				while(rsTemp.next()) {
					alben.add(new Album(rsTemp.getInt("id"), rsTemp.getString("name")));
					length++;
				}
				rsTemp.close();
			} catch (SQLException e) {
				e.printStackTrace();
				return;
			}
			
			alb =  new Album[length+1][1];
			alb[0][0]=(new Album(0, "Alle("+length+")"));
			length++;
			for (int i=1; i<length; i++) {
				alb[i][0] = alben.get(i-1);
				
			}
			//alben-tabelle aktualisieren
			String[] cols = {"Alben"};
			DefaultTableModel model = new DefaultTableModel(alb, cols);
			albums.setModel(model);
			//track-table aktualisieren.
			List<Track> tracks = new ArrayList<Track>();
			rsTemp = PhoenixCore.DBCON.executeOnLib("select t.id, t.name, ar.id as artistID, t.album as albumID, " +// und alle titel holen
					"t.genre, t.track, t.length, t.checked, t.path, ar.name as artistName, al.name as albumName " +
					"from tracks t inner join albums al on t.album=al.id inner join artists ar on al.artist=ar.id " +
					" order by lower(artistName), lower(albumName), t.track asc");
			try {
				while(rsTemp.next()) {
//					tracks.add(new Track(rsTemp.getInt("id"), rsTemp.getString("name"), 
//							rsTemp.getString("artistName"), rsTemp.getString("albumName"), rsTemp
//							.getString("genre"), rsTemp.getString("track"), rsTemp
//							.getInt("length"), rsTemp.getInt("artistID"), rsTemp
//							.getInt("albumID"), rsTemp.getInt("checked") == 1 ? true
//							: false, rsTemp.getString("path")));
					tracks.add(new Track(rsTemp.getInt("id")));
				}
				rsTemp.close();
			} catch (SQLException e) {
				e.printStackTrace();
				return;
			}
			((TrackTable)trackTable).setTracksInLib(tracks);
		}else {//ein "echter" interpret wurde selektiert.
			rsTemp = PhoenixCore.DBCON.executeOnLib("SELECT al.id, al.name from albums as al " +//alben vom interpreten holen
					"inner join artists as a on a.id = al.artist where al.artist = "
					+selectedArtist.getId()+" order by lower(al.name) asc");
			try {
				
				while(rsTemp.next()) {
					alben.add(new Album(rsTemp.getInt("id"), rsTemp.getString("name")));
					length++;
				}
				rsTemp.close();
			} catch (SQLException e) {
				e.printStackTrace();
				return;
			}
			
			alb =  new Album[length+1][1];
			alb[0][0]=(new Album(0, "Alle("+length+")", selectedArtist.getId(), selectedArtist.getName()));
			length++;
			for (int i=1; i<length; i++) {
				alb[i][0] = alben.get(i-1);
			}
			//alben-tabelle aktualisieren
			String[] cols = {"Alben"};
			DefaultTableModel model = new DefaultTableModel(alb, cols);
			albums.setModel(model);
			albums.setFillsViewportHeight(true);
			//albums.repaint();
			((JViewport)albums.getParent()).setViewPosition(new Point(0, 0));
			//track-table aktualisieren.
			List<Track> tracks = new ArrayList<Track>();
			rsTemp = PhoenixCore.DBCON.executeOnLib("select t.id, t.name, ar.id as artistID, t.album as albumID, " +//titel vom interpreten holen
					"t.genre, t.track, t.length, t.checked, t.path, ar.name as artistName, al.name as albumName " +
					"from tracks t inner join albums al on t.album=al.id inner join artists ar on al.artist=ar.id " +
					"where ar.name='"+selectedArtist.getName().replace("'", "''")+"' order by lower(albumName), t.track asc");
			try {
				while(rsTemp.next()) {
//					tracks.add(new Track(rsTemp.getInt("id"), rsTemp.getString("name"), 
//							rsTemp.getString("artistName"), rsTemp.getString("albumName"), rsTemp
//							.getString("genre"), rsTemp.getString("track"), rsTemp
//							.getInt("length"), rsTemp.getInt("artistID"), rsTemp
//							.getInt("albumID"), rsTemp.getInt("checked") == 1 ? true
//							: false, rsTemp.getString("path")));
					tracks.add(new Track(rsTemp.getInt("id")));
				}
				rsTemp.close();
			} catch (SQLException e) {
				e.printStackTrace();
				return;
			}
			trackTable.setTracksInLib(tracks);
			Runtime.getRuntime().gc();
		}
	}
	public void albumSelected(Album selectedAlbum){
		ResultSet rsTemp=null;
		List<Track> tracks = new ArrayList<Track>();
		if (selectedAlbum.getId()==0) {// album-id = 0 -> "alle" wurde selektiert
			if (selectedAlbum.getArtistID()==0) {//kein spezieller artist ausgewählt -> alle alben anzeigen
				rsTemp = PhoenixCore.DBCON.executeOnLib("select t.id  " +
						"from tracks t inner join albums al on t.album=al.id inner join artists ar on al.artist=ar.id " +//titel vom interpreten holen
						"order by lower(ar.name), lower(al.name), t.track asc");
			}else {//nur alben des künstlers anzeigen
				rsTemp = PhoenixCore.DBCON.executeOnLib("select t.id " +
						"from tracks t inner join albums al on t.album=al.id inner join artists ar on al.artist=ar.id " +//titel vom interpreten holen
						"where ar.name='"+selectedAlbum.getArtistName().replace("'", "''")+"' order by lower(al.name), t.track asc");
			}
			
		}else {//"echtes" album wurde selektiert
			rsTemp = PhoenixCore.DBCON.executeOnLib("select t.id " +
					"from tracks t inner join albums al on t.album=al.id inner join artists ar on al.artist=ar.id " +//alle titel vom interpreten holen
					"where al.name='"+selectedAlbum.getName().replace("'", "''")+"' order by lower(al.name), t.track asc");
		}
		
		try {
			while(rsTemp.next()) {
				tracks.add(new Track(rsTemp.getInt("id")));
			}
			rsTemp.close();
		} catch (SQLException e) {
			e.printStackTrace();
			return;
		}
		trackTable.setTracksInLib(tracks);
	}

	
}