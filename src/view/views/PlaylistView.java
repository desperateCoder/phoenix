package view.views;

import javax.swing.JScrollPane;

import model.Library;
import model.Track;
import view.AbstractView;
import view.TrackTable;
import controller.PhoenixCore;
import controller.threads.PlaylistTrackAdder;

@SuppressWarnings("serial")
public class PlaylistView extends AbstractView {
	private TrackTable trackTable;
	private int id =0;
	private String name = "Empty";
	public PlaylistView(PhoenixCore core,int id, String name) {
		super(name);
		this.name = name;
		this.id = id;
		lib = new Library(id, name);
		trackTable = new TrackTable(lib, core, id);
		add(new JScrollPane(trackTable, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER));
	}


	@Override
	public void setPlaying(Track t, int index) {
		trackTable.setPlaying(t, index);
	}

	@Override
	public void addTracks(Track[] tracks) {
		Thread addThread = new Thread(new PlaylistTrackAdder(tracks, id, lib.getTrackCount()));
		addThread.start();
		trackTable.addTracks(tracks);
	}
	public int getPlaylistID() {
		return this.id;
	}public String getPlaylistName() {
		return this.name;
	}
	@Override
	public String toString() {
		return this.name;
	}
	public void setPlaylistName(String name){
		this.name = name;
	}
}
