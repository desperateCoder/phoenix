package view.views;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;

import model.Library;
import model.TaggerTrackTableModel;
import model.Track;
import view.AbstractView;
import view.customs.TaggerEditForm;
import view.customs.TaggerTrackTable;
import controller.PhoenixCore;
/**
 * die Ansicht des Taggers
 * @author Artur Dawtjan
 */
@SuppressWarnings("serial")
public class TaggerView extends AbstractView implements ActionListener {
	JSplitPane split;
	TaggerTrackTable trackTable;
	PhoenixCore core;
	TaggerEditForm form;
	public TaggerView(String name, PhoenixCore core) {
		super(name);
		this.core = core;
		split = new JSplitPane();
		setLayout(new BorderLayout());
		//linke seite
		trackTable = new TaggerTrackTable(this, core);
		trackTable.setFillsViewportHeight(true);
		split.setLeftComponent( new JScrollPane(trackTable, 
				JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, 
				JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED));
		split.setResizeWeight(0.5);
		split.setDividerLocation(0.5);
		//rechte seite
		form = new TaggerEditForm((TaggerTrackTableModel)trackTable.getModel());
		split.setRightComponent(new JScrollPane(form, 
							JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
							JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED));
		add(split, BorderLayout.CENTER);
		JButton save = new JButton("Speichern");
		save.addActionListener(this);
		add(save, BorderLayout.SOUTH);
		
	}

	@Override
	public void setPlaying(Track t, int index) {
		trackTable.setPlaying(t, index);
	}

	@Override
	public void addTracks(Track[] tracks) {
		trackTable.addTracks(tracks);
	}
	@Override
	public Library getLibrary() {
		return trackTable.getLibrary();
	}

	public void setSelectedRows(Track[] selectedTracks, int sel[]) {
		form.setSelectedTracks(selectedTracks, sel);
		
	}

	public void actionPerformed(ActionEvent e) {
		trackTable.writeTags();
	}
}
