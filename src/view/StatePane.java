package view;

import java.awt.BorderLayout;

import javax.swing.JLabel;
import javax.swing.JPanel;

import controller.PhoenixCore;

@SuppressWarnings("serial")
public class StatePane extends JPanel {
	private JLabel info;
	private JLabel log;
	private ProgressPane progress;
	private PhoenixCore core;
	public JLabel getInfo() {
		return info;
	}
	public JLabel getLog() {
		return log;
	}
	public ProgressPane getProgress() {
		return progress;
	}
	public PhoenixCore getCore() {
		return core;
	}
	public StatePane(PhoenixCore core) {
		setLayout(new BorderLayout());
		this.core = core;
		add(new JLabel("statuszeile"), BorderLayout.CENTER);
	}
	
}
