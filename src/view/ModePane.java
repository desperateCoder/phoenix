package view;

import java.awt.Dimension;

import javax.swing.JPanel;

import controller.interfaces.PlayModeChangedListener;

@SuppressWarnings("serial")
public class ModePane extends JPanel {
	
	public ModePane(PlayModeChangedListener l) {
		add(new ModeToggleButtonGroup(l));
		setPreferredSize(new Dimension(320, 10));
	}
}
