package view;

import java.awt.Dimension;

import javax.swing.JButton;
import javax.swing.JPanel;

import model.constants.ActionCommands;
import model.constants.SettingKeys;
import controller.PhoenixCore;
/**
 * Lautstärkeregler 
 * @author Artur Dawtjan
 *
 */
@SuppressWarnings("serial")
public class VolumeSlider extends JPanel {
	PhoenixCore core;
	ClickSlider volumeSlider;
	public VolumeSlider(PhoenixCore core) {
		this.core = core;
		int initVol = core.getSettings().getSettingAsInt(SettingKeys.PLAYER_VOLUME);
		initVol=initVol>100?100:initVol<0?0:initVol;//initVol MUSS zwischen 0 und 100 liegen!
		volumeSlider = new ClickSlider(0, 100, initVol);
		volumeSlider.setFocusable(false);
		volumeSlider.addChangeListener(core);
		volumeSlider.setMajorTickSpacing(50);
		volumeSlider.setMinorTickSpacing(25);
		volumeSlider.setPaintTicks(true);
		volumeSlider.setPreferredSize(new Dimension(100, 30));
		
		JButton minBtn = new JButton(".");
		minBtn.setFocusable(false);
		minBtn.setActionCommand(ActionCommands.VOLUME_MUTE);
		minBtn.addActionListener(core);
		
		JButton maxBtn = new JButton("...");
		maxBtn.setFocusable(false);
		maxBtn.setActionCommand(ActionCommands.VOLUME_MAX);
		maxBtn.addActionListener(core);
		
		add(minBtn);
		add(volumeSlider);
		add(maxBtn);
	}
	public void mute() {
		volumeSlider.setValueTo(0);
	}
	public void maxVolume() {
		volumeSlider.setValueTo(100);
	}
	public int getVolumeValue() {
		return volumeSlider.getValue();
	}
}
