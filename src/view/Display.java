package view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;

import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

import model.Track;
import controller.PhoenixCore;
/**
 * Display oben
 * @author Artur Dawtjan
 *
 */
@SuppressWarnings("serial")
public class Display extends JPanel {
	private JLabel title, artistAndAlbum, elapsed, remeaning;
	
	ClickSlider seekSlider;
	Track currTrack;
	public Display(PhoenixCore core) {
		title = new JLabel("Keine Wiedergabe");
		title.setFont(new Font(Font.SANS_SERIF, Font.BOLD, 14));
		title.setAlignmentX(JLabel.CENTER_ALIGNMENT);
		title.setFocusable(false);
		artistAndAlbum = new JLabel(" ");
		artistAndAlbum.setFocusable(false);
		title.setHorizontalAlignment(SwingConstants.CENTER);
		artistAndAlbum.setHorizontalAlignment(SwingConstants.CENTER);
		artistAndAlbum.setAlignmentX(JLabel.CENTER_ALIGNMENT);
		elapsed = new JLabel("");
		elapsed.setFocusable(false);
		remeaning = new JLabel("");
		remeaning.setFocusable(false);
		seekSlider = new ClickSlider(core.getJukebox().getRangeModel(), this);
		seekSlider.setFocusable(false);
		setLayout(new BorderLayout());
		JPanel titlePanel = new JPanel();
		titlePanel.setLayout(new BoxLayout(titlePanel, BoxLayout.Y_AXIS));
		titlePanel.add(title);
		titlePanel.add(artistAndAlbum);
		add(titlePanel, BorderLayout.NORTH);
		add(seekSlider, BorderLayout.CENTER);
		add(elapsed,BorderLayout.WEST);
		add(remeaning, BorderLayout.EAST);
		setBorder(new LineBorder(Color.GRAY, 1, true));
	}
	
	/**
	 * track setzen
	 * @param t neuer Track
	 */
	public void setTrack(Track t){
		currTrack = t;
		title.setText(t.getTitle());
		String name = !t.getArtistName().equals("")?"von "+t.getArtistName():"";
		String album = t.getAlbumName().equals("")?"":" aus "+t.getAlbumName();
		artistAndAlbum.setText(name+album);
	}
	/**
	 * setzt den die Strings für die Dauer-Label
	 * @param el zeit, die schon vergangen ist in sec
	 * @param rem zeit, die noch bleibt in sec
	 */
	public void setLengthString(int el, int rem){
		this.elapsed.setText(Track.intToStringLength(el));
		this.remeaning.setText(Track.intToStringLength(rem));
	}
}
