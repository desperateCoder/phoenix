package view;

import java.awt.event.KeyListener;

import javax.swing.JScrollPane;
import javax.swing.JTextArea;

@SuppressWarnings("serial")
public class LyrixDisplay extends JScrollPane{
	JTextArea area;
	public LyrixDisplay(KeyListener l) {
		super(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		area = new JTextArea();
		area.setEditable(false);
		area.setLineWrap(true);
		area.setWrapStyleWord(true);
		setViewportView(area);
		area.setFocusable(false);
	}
	public void setText(String lyrix) {
		area.setText(lyrix);
		getVerticalScrollBar().setValue(0);
	}
	
}
