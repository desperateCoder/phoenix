package view;

import java.awt.Component;
import java.awt.Graphics;

import javax.swing.JSplitPane;

@SuppressWarnings("serial")
public class StickySplitPane extends JSplitPane {
	public static final boolean STICK_RIGHT=false;
	public static final boolean STICK_LEFT=true;
	
	private boolean stickTo = StickySplitPane.STICK_LEFT;
	
	public StickySplitPane(boolean stickySide) {
		this();
		stickTo = stickySide;
	}
	public StickySplitPane() {
		super(JSplitPane.HORIZONTAL_SPLIT);
	}
	
	/**
	 * Setzt die seite, die gleichbleiben soll. 
	 * Entsprechende Konstanten sind in dieser klasse vorhanden
	 * @param direction
	 */
	public void setStickySide(boolean side){
		this.stickTo = side;
	}
	
	/**
	 * überschrieben, da die sticky-seite ihre breite halten muss. 
	 */
	@Override
	public void paint(Graphics g) {
		System.out.println("breite: "+getStickyComponent().getWidth());
		super.paint(g);
	}
	
	private Component getStickyComponent(){
		return stickTo ? getLeftComponent() : getRightComponent();
	}
}
