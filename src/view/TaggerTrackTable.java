package view;

import javax.swing.JTable;

import model.TaggerTrackTableModel;
import controller.interfaces.customs.TrackTransferHandler;

@SuppressWarnings("serial")
public class TaggerTrackTable extends JTable{
	public TaggerTrackTable() {
		setModel(new TaggerTrackTableModel());
		setTransferHandler(new TrackTransferHandler());
	}
}
