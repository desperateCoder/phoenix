package view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.border.LineBorder;
import javax.swing.plaf.basic.BasicSplitPaneDivider;
import javax.swing.plaf.basic.BasicSplitPaneUI;

import model.Library;
import model.Track;
import model.constants.ActionCommands;
import model.constants.SettingKeys;
import controller.LyrixCrawler;
import controller.PhoenixCore;
import controller.interfaces.PhoenixGUI;
import controller.listener.DividerLocationPropertyChangeListener;

/**
 * Default-skin klasse (skins implementieren immer PhoenixGUI)
 * 
 * @author Artur Dawtjan
 * 
 */
@SuppressWarnings("serial")
public class Phoenix extends PhoenixGUI {
	/**
	 * Referenz auf Kernel
	 */
	private PhoenixCore core;

	/**
	 * PlayControls
	 */
	private PlayControlPanel controlPanel;
	/**
	 * Lyric-Suche
	 */
	private LyrixCrawler lyrixCrawler;
	/**
	 * Display
	 */
	private Display display;
	/**
	 * lyrics-anzeige
	 */
	private LyrixDisplay lyrixDisplay;
	/**
	 * links: die View. rechts: lyrics
	 */
	private JSplitPane innerSplit;
	private AbstractView currentShownView;
	private AbstractView currentPlayedView;
	private LibTreeView treeView;

	public Phoenix(PhoenixCore core) {
		this.core = core;
		// Look And Feel setzen
		try {
			UIManager.put("control", new Color(155, 155, 155));
			UIManager.put("nimbusBlueGrey", new Color(170, 170, 170));
			UIManager.put("nimbusBase", new Color(122, 122, 122));
			// UIManager.put("ScrollBar.background", new Color(255,182,46));
			UIManager
					.setLookAndFeel("javax.swing.plaf.nimbus.NimbusLookAndFeel");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (UnsupportedLookAndFeelException e) {
			e.printStackTrace();
		}
		// Icon
		setIconImage(java.awt.Toolkit.getDefaultToolkit().getImage(
				"img/phoenix_100x100.png"));

		// container für alles, was im fenster liegt
		JPanel containerPanel = new JPanel();
		containerPanel.setLayout(new BorderLayout());

		// Menu-Bar zusammenbauen
		JMenuBar menuBar = new JMenuBar();
		menuBar.setForeground(new Color(255, 0, 0));
		JMenu fileMenu = new JMenu("Datei");
		JMenuItem menuLoadFolder = new JMenuItem("Ordner importieren");
		menuLoadFolder.setActionCommand(ActionCommands.FILE_IMPORT_FOLDER);
		menuLoadFolder.addActionListener(core);
		fileMenu.add(menuLoadFolder);
		menuBar.add(fileMenu);
		setJMenuBar(menuBar);

		// oberer teil des fensters
		JPanel headPanel = new JPanel();
		headPanel.setLayout(new BorderLayout());
		controlPanel = new PlayControlPanel(core, core);
		headPanel.add(controlPanel, BorderLayout.WEST);
		display = new Display(core);
		headPanel.add(display, BorderLayout.CENTER);
		headPanel.add(new ModePane(core), BorderLayout.EAST);

		// hauptpanel
		JSplitPane mainPanel = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT);
		mainPanel.setBorder(new LineBorder(Color.gray, 1));
		innerSplit = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT);
		treeView = new LibTreeView(this);
		mainPanel.add(new JScrollPane(treeView,
				JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
				JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED));
		innerSplit.setResizeWeight(1);

		lyrixDisplay = new LyrixDisplay(core);
		innerSplit.add(lyrixDisplay);
		// doppelklick auf Divider
		BasicSplitPaneUI ui = (BasicSplitPaneUI) innerSplit.getUI();
		ui.getDivider().addMouseListener(new MouseListener() {
			public void mouseReleased(MouseEvent e) {
			}

			public void mousePressed(MouseEvent e) {
			}

			public void mouseExited(MouseEvent e) {
			}

			public void mouseEntered(MouseEvent e) {
			}

			public void mouseClicked(MouseEvent e) {
				if (e.getClickCount() == 2) { // bei doppelklick einklappen
					JSplitPane source = ((JSplitPane) ((BasicSplitPaneDivider) e
							.getSource()).getParent());
					source.setDividerLocation(1.0);
				}
			}
		});
		mainPanel.add(innerSplit);
		containerPanel.add(headPanel, BorderLayout.NORTH);
		containerPanel.add(mainPanel, BorderLayout.CENTER);

		// Statusbar unten
		StatePane statePane = new StatePane(core);
		containerPanel.add(statePane, BorderLayout.SOUTH);

		getContentPane().addKeyListener(core);

		mainPanel.setDividerLocation(PhoenixCore.INSTANCE.getSettings()
				.getSettingAsInt(SettingKeys.SPLIT_FRAME_MAIN));
		mainPanel
				.addPropertyChangeListener(new DividerLocationPropertyChangeListener(
						SettingKeys.SPLIT_FRAME_MAIN, mainPanel));
		innerSplit.setDividerLocation(PhoenixCore.INSTANCE.getSettings()
				.getSettingAsInt(SettingKeys.SPLIT_FRAME_INNER));
		innerSplit
				.addPropertyChangeListener(new DividerLocationPropertyChangeListener(
						SettingKeys.SPLIT_FRAME_INNER, innerSplit));
		// groeße wiederherstellen
		int h = PhoenixCore.INSTANCE.getSettings().getSettingAsInt(
				SettingKeys.FRAME_HEIGHT);
		int w = PhoenixCore.INSTANCE.getSettings().getSettingAsInt(
				SettingKeys.FRAME_WIDTH);
		// setPreferredSize(new Dimension(w, h));
		setSize(w, h);
		// position wiederherstellen
		int x = PhoenixCore.INSTANCE.getSettings().getSettingAsInt(
				SettingKeys.FRAME_POSITION_X);
		int y = PhoenixCore.INSTANCE.getSettings().getSettingAsInt(
				SettingKeys.FRAME_POSITION_Y);
		setLocation(x, y);

		// component-listener: bei ändern der position oder groeße in db
		// eintragen (settings)
		addComponentListener(new ComponentListener() {
			private int waitTime = 0;
			private Map<Integer, Integer> values = new HashMap<Integer, Integer>();
			private Runnable setterThread = new Runnable() {

				@Override
				public void run() {
					while (waitTime > 0) {
						try {
							Thread.sleep(100);
							waitTime -= 100;
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
					}
					Set<Integer> keys = values.keySet();
					for (Iterator<Integer> it = keys.iterator(); it.hasNext();) {
						Integer i = it.next();
						PhoenixCore.INSTANCE.getSettings().setSetting(i,
								values.get(i) + "");
//						System.out.println("value " + i + " written!");
					}
					values.clear();
				}
			};

			public void componentResized(ComponentEvent evt) {
				Component c = (Component) evt.getSource();
				Dimension d = c.getSize();

				values.put(SettingKeys.FRAME_HEIGHT, (int) d.getHeight());
				values.put(SettingKeys.FRAME_WIDTH, (int) d.getWidth());

				if (waitTime < 1) {
//					System.out.println("thread " + SettingKeys.FRAME_HEIGHT
//							+ " + " + SettingKeys.FRAME_WIDTH + " started");
					waitTime = 1000;
					new Thread(setterThread).start();
				} else {
					waitTime = 1000;
				}
			}

			@Override
			public void componentMoved(ComponentEvent e) {
				JFrame c = (JFrame) e.getSource();
				Point d = c.getLocation();

				values.put(SettingKeys.FRAME_POSITION_X, d.x);
				values.put(SettingKeys.FRAME_POSITION_Y, d.y);

				if (waitTime < 1) {
					waitTime = 1000;
					new Thread(setterThread).start();
				} else {
					waitTime = 1000;
				}
			}

			@Override
			public void componentShown(ComponentEvent e) {
			}

			@Override
			public void componentHidden(ComponentEvent e) {
			}
		});

		// Standard-zeug
		getContentPane().add(containerPanel);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setTitle("Phoenix");

		setVisible(true);
	}

	/**
	 * Liefert ein ImageIcon wenn die ressource gefunden wird, andernfalls null.
	 */
	protected ImageIcon createImageIcon(String path, String description) {
		java.net.URL imgURL = getClass().getResource(path);
		if (imgURL != null) {
			return new ImageIcon(imgURL, description);
		} else {
			System.err.println("Datei nicht gefunden: " + path);
			return null;
		}
	}

	public Library getCurrentQueueLib() {
		return treeView.getQueue();
	}

	public void mute() {
		controlPanel.mute();
	}

	public void maxVolume() {
		controlPanel.maxVolume();
	}

	public int getVolume() {
		return controlPanel.getVolume();
	}

	public PhoenixCore getCore() {
		return core;
	}

	public void jukeboxStateChanged(int state) {
		controlPanel.setState(state);
	}

	public void setView(AbstractView view) {
		int position = -1;
		if (innerSplit.getLeftComponent() != null) {// position merken
			position = innerSplit.getDividerLocation();
		}

		innerSplit.setLeftComponent(view);// view setzen
		if (position >= 0) {
			innerSplit.setDividerLocation(position);// und wiederherstellen
		}
		currentShownView = view;
	}

	/**
	 * Track hat sich geändert
	 */
	public void trackChanged(Track t, int index) {
		if (currentPlayedView == null) {
			currentPlayedView = currentShownView;
		}
		try {
			currentPlayedView.setPlaying(t, index);
		} catch (Exception e) {
			e.printStackTrace();
		}

		display.setTrack(t);
		lyrixCrawler = new LyrixCrawler(t.getArtistName(), t.getTitle(), this);
		Thread crawler = new Thread(lyrixCrawler);
		crawler.start();
	}

	/**
	 * setzt die lyrics im lyrix-panel
	 */
	public void setLyrix(String lyrix) {
		lyrixDisplay.setText(lyrix);
	}

	public Library getCurrentPlayedLib() {
		if (currentPlayedView == null) {
			currentPlayedView = currentShownView;
		}
		return currentPlayedView.getLibrary();
	}

	public void currentViewPlayed() {
		currentPlayedView = currentShownView;
	}

	public void updateQueueView() {
		treeView.updateQueueView();
	}

	@Override
	public void refreshLibrary() {
		treeView.refreshLibrary();
	}

}
