package view;

import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JPanel;

import controller.PhoenixCore;
import controller.interfaces.JukeboxStateListener;

import model.constants.ActionCommands;
/**
 * Container für die abspiel-buttons
 * @author artur
 *
 */
@SuppressWarnings("serial")
public class PlayControlPanel extends JPanel {
	JButton play;
	VolumeSlider volumeSlider;
	public PlayControlPanel(ActionListener l, PhoenixCore core) {
		//prev-Button
		JButton prev = new JButton("<");
		prev.setActionCommand(ActionCommands.PREV);
		prev.addActionListener(l);
		prev.setFocusable(false);
		//play-button
		play = new JButton("I>");
		play.setActionCommand(ActionCommands.PLAY);
		play.addActionListener(l);
		play.setFocusable(false);
		//next-button
		JButton next = new JButton(">");
		next.setActionCommand(ActionCommands.NEXT);
		next.addActionListener(l);
		next.setFocusable(false);
		
		volumeSlider = new VolumeSlider(core);
		
		add(prev);
		add(play);
		add(next);
		add(volumeSlider);
	}

	public void setState(int state) {
		if (state == JukeboxStateListener.PLAYING) {
			play.setText("II");
		} else if (state == JukeboxStateListener.PAUSED) {
			play.setText("I>");
		}
	}
	public void mute() {
		volumeSlider.mute();
	}

	public void maxVolume() {
		volumeSlider.maxVolume();
	}

	public int getVolume() {
		return volumeSlider.getVolumeValue();
	}

}
