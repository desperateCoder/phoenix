package view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JPanel;
import javax.swing.JToggleButton;

import model.constants.SettingKeys;

import controller.Jukebox;
import controller.PhoenixCore;
import controller.interfaces.PlayModeChangedListener;
/**
 * ToggleButtons für den playmode.
 * @author Artur Dawtjan
 *
 */
@SuppressWarnings("serial")
public class ModeToggleButtonGroup extends JPanel implements ActionListener {
	private static final String RANDOM_PRESSED = "1";
	private static final String REPEAT_PRESSED = "2";
	private static final String REPEAT_ALL_PRESSED = "3";
	private JToggleButton random;
	private JToggleButton repeat;
	private JToggleButton repeatAll;
	private int playMode = Jukebox.NO_REPEAT_MODE;
	private ArrayList<PlayModeChangedListener> modeChangedListener = new ArrayList<PlayModeChangedListener>();
	
	
	public ModeToggleButtonGroup(PlayModeChangedListener l) {
		//Random-Button
		random = new JToggleButton("X");
		random.setToolTipText("Zufällige Titel wiedergeben");
		random.setActionCommand(ModeToggleButtonGroup.RANDOM_PRESSED);
		random.addActionListener(this);
		random.setFocusable(false);
		//Repeat-Button
		repeat = new JToggleButton("8(1)");
		repeat.setToolTipText("Einen Titel in Endlosschleife");
		repeat.setActionCommand(ModeToggleButtonGroup.REPEAT_PRESSED);
		repeat.addActionListener(this);
		repeat.setFocusable(false);
		//Repeat-All-Button
		repeatAll = new JToggleButton("8");
		repeatAll.setToolTipText("Liste in Endlosschleife");
		repeatAll.setActionCommand(ModeToggleButtonGroup.REPEAT_ALL_PRESSED);
		repeatAll.addActionListener(this);
		repeatAll.setFocusable(false);
		//listener
		modeChangedListener.add(l);
		
		add(repeat);
		add(random);
		add(repeatAll);
		
		int mode = PhoenixCore.INSTANCE.getSettings().getSettingAsInt(SettingKeys.PLAYER_MODE);
		switch (mode) {
		case Jukebox.REPEAT_ALL_MODE:
			repeatAll.doClick();
			break;
		case Jukebox.REPEAT_MODE:
			repeat.doClick();
			break;
		case Jukebox.SHUFFLE_MODE:
			random.doClick();
			break;
		}
	}
	
	/**
	 * konstruktor, der gleich playmode mit übernimmt.
	 * @param playMode
	 */
	public ModeToggleButtonGroup(PlayModeChangedListener l, int playMode){
		this(l);
		this.playMode = playMode;
	}
	public int getPlayMode(){
		return playMode;
	}
	
	private void setMode(int newMode){
		playMode = newMode;
		PhoenixCore.INSTANCE.getSettings().setSetting(SettingKeys.PLAYER_MODE, newMode+"");
		
		for (PlayModeChangedListener l : modeChangedListener) {
			l.modeChanged(newMode);
		}
	}

	public void actionPerformed(ActionEvent e) {
		String c = e.getActionCommand();
		if (c.equals(ModeToggleButtonGroup.RANDOM_PRESSED)) {
			if (playMode==Jukebox.SHUFFLE_MODE) {
				setMode(Jukebox.NO_REPEAT_MODE);
			} else{
				repeat.setSelected(false);
				repeatAll.setSelected(false);
				setMode(Jukebox.SHUFFLE_MODE);
			}
		}else if (c.equals(ModeToggleButtonGroup.REPEAT_ALL_PRESSED)) {
			if (playMode==Jukebox.REPEAT_ALL_MODE) {
				setMode(Jukebox.NO_REPEAT_MODE);
			} else{
				repeat.setSelected(false);
				random.setSelected(false);
				setMode(Jukebox.REPEAT_ALL_MODE);
			}
		}else if (c.equals(ModeToggleButtonGroup.REPEAT_PRESSED)) {
			if (playMode==Jukebox.REPEAT_MODE) {
				setMode(Jukebox.NO_REPEAT_MODE);
			} else{
				random.setSelected(false);
				repeatAll.setSelected(false);
				setMode(Jukebox.REPEAT_MODE);
			}
		}
	}
}
