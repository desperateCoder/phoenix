package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JSlider;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import model.Library;
import model.Settings;
import model.Track;
import model.constants.ActionCommands;
import view.Phoenix;
import controller.interfaces.JukeboxChangedTrackListener;
import controller.interfaces.JukeboxStateListener;
import controller.interfaces.PhoenixGUI;
import controller.interfaces.PlayModeChangedListener;

/** Hauptklasse. Dieses monstrum steuert alles andere. Gui ist nur ein layer, der drüber liegt.
 * 
 * @author Artur Dawtjan
 *
 */
public class PhoenixCore implements ActionListener, ChangeListener, JukeboxStateListener, JukeboxChangedTrackListener, PlayModeChangedListener, KeyListener{
	/**
	 * Aktullelles Gui.
	 */
	private PhoenixGUI gui = null;
	/**
	 * DB-Anbindung
	 */
	public static final SQLiteConnection DBCON = new SQLiteConnection();
	/**
	 * Referenz auf Core-Singleton
	 */
	public static PhoenixCore INSTANCE = null;
	/**
	 * gespeicherte einstellungen
	 */
	private Settings settings = null;
	/**
	 * Jukebox, organisiert die play-reihenfolge
	 */
	private Jukebox jukebox = null;
	/**
	 * Crawler für audiodateien. überwacht und importiert.
	 */
	private AudioCrawler crawler;
	/**
	 * Konstruktor
	 */
	public PhoenixCore() {
		PhoenixCore.INSTANCE = this;
		//module initialisieren
		//	Einstellungen
		settings = new Settings();
		//  Jukebox
		jukebox = new Jukebox(this);
		//  Crawler
		crawler = new AudioCrawler();
		
		
		
		//zum schluss
		mask();
		
	}
	
	/**
	 *  Maskiert den kernel mit der gui
	 */
	private void mask(){
		//später mal einstellungen prüfen und entsprechenden skin anziehen. default ist view.Phoenix.java
		//z.B. .class-files aus dateisystem laden:
		// Create a File object on the root of the directory containing the class file
//		File file = new File("c:\\myclasses\\");
//
//		try {
//		    // Convert File to a URL
//		    URL url = file.toURL();          // file:/c:/myclasses/
//		    URL[] urls = new URL[]{url};
//
//		    // Create a new class loader with the directory
//		    ClassLoader cl = new URLClassLoader(urls);
//
//		    // Load in the class; MyClass.class should be located in
//		    // the directory file:/c:/myclasses/com/mycompany
//		    Class cls = cl.loadClass("com.mycompany.MyClass");
//		} catch (MalformedURLException e) {
//		} catch (ClassNotFoundException e) {
//		}
		this.gui = new Phoenix(this);
	}
	
	public static void main(String[] args) {
		new PhoenixCore();
	}
	
	/**
	 * Anlaufstelle für Gui-getriebene Events
	 */
	public void actionPerformed(ActionEvent e) {
		String command = e.getActionCommand();
		if (command.equals(ActionCommands.PLAY)) {
			if (e.getID() ==  ActionCommands.PLAY_LIB) {
				jukebox.play(getLib());
			}else if (jukebox.isPlaying()) {
				jukebox.pause();
			}else if (jukebox.hasFinnished()) {
				jukebox.play(getLib());
			}else jukebox.play();
		} else if (command.equals(ActionCommands.NEXT)){
			if (jukebox.hasFinnished()&&!jukebox.isPlaying()) { //falls nicht am Abspielen:
				jukebox.play(getLib());//vorne anfangen
				return;
			}
			jukebox.next();
		}else if (command.equals(ActionCommands.PREV)){
			if (jukebox.hasFinnished()&&!jukebox.isPlaying()) {//falls nicht am Abspielen:
				jukebox.play(getLib());//vorne anfangen.
				return;
			}
			jukebox.prev();
		}else if (command.equals(ActionCommands.STOP)){

		}else if (command.equals(ActionCommands.FILE_IMPORT_FOLDER)){
			crawler.importFolder();
		}else if (command.equals(ActionCommands.VOLUME_MUTE)){
			gui.mute();
		}else if (command.equals(ActionCommands.VOLUME_MAX)){
			gui.maxVolume();
		}
	}
	/**
	 * @return die aktuelle, in der jukebox abzuspielende library
	 */
	public Library getLib() {
		return gui.getCurrentPlayedLib();
	}
	public Jukebox getJukebox() {
		return jukebox;
	}

	public Settings getSettings() {
		return settings;
	}
	public int getVolume(){
		return gui.getVolume();
	}
	public void refreshLibrary(){
		gui.refreshLibrary();
	}
	/**
	 * füllt die jukebox mit titeln und setzt den startpunkt
	 * @param tracks 
	 * @param row row-index (anfangs-index) aus der tabelle
	 */
	public void fillJukebox(Library tracks, int row) {
		jukebox.play(tracks, row);
		gui.currentViewPlayed();
	}
	/**
	 * z.B. wenn lautstärke geändert wurde.
	 */
	public void stateChanged(ChangeEvent e) {
		 JSlider source = (JSlider)e.getSource();
		 jukebox.setVolume(source.getValue());
	}
	/**
	 * Wenn die wiedergabe gestartet wird oder aufhört.
	 */
	public void jukeboxStateChanged(int state) {
		gui.jukeboxStateChanged(state);
	}
	/**
	 * wenn das wiederzugebende lied sich ändert
	 */
	public void trackChanged(Track t) {
		gui.trackChanged(t, getLib().indexOf(t));
	}
	/**
	 * Abspielmodus wurde geändert
	 */
	public void modeChanged(int newMode) {
		jukebox.setPlayMode(newMode);
	}

	// vom keyListener
	public void keyPressed(KeyEvent e) {
		int code = e.getKeyCode();
		if (code==KeyEvent.VK_SPACE) { // auf space klick auf playbutton simulieren.
			actionPerformed(new ActionEvent(this, 1, ActionCommands.PLAY));
		} else if (code == KeyEvent.VK_RIGHT) {//pfeil rechts -> nächstes lied
			actionPerformed(new ActionEvent(this, 1, ActionCommands.NEXT));
		} else if (code == KeyEvent.VK_LEFT) {//pfeil links -> vorheriges lied
			actionPerformed(new ActionEvent(this, 1, ActionCommands.PREV));
		}
	}
	public void keyReleased(KeyEvent e) {}

	public void keyTyped(KeyEvent e) {}

	public Library getQueueLib() {
		return gui.getCurrentQueueLib();
	}

	public void updateQueue() {
		gui.updateQueueView();
		
	}

	public PhoenixGUI getCurrentGUI() {
		return this.gui;
	}


}
