package controller;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;

import javax.swing.JFileChooser;

import model.TrackDto;

import org.jaudiotagger.audio.AudioFile;
import org.jaudiotagger.audio.AudioFileIO;
import org.jaudiotagger.audio.exceptions.CannotReadException;
import org.jaudiotagger.audio.exceptions.InvalidAudioFrameException;
import org.jaudiotagger.audio.exceptions.ReadOnlyFileException;
import org.jaudiotagger.tag.FieldKey;
import org.jaudiotagger.tag.Tag;
import org.jaudiotagger.tag.TagException;

public class FolderImporter implements Runnable {
	private File rootDir;
	private TrackToDBQueue queue;
	public FolderImporter() {
	}

	public void run() {
		JFileChooser fc = new JFileChooser();
		fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		int returnVal = fc.showOpenDialog(null);

		if (returnVal == JFileChooser.APPROVE_OPTION) {
			rootDir = fc.getSelectedFile();
			grabAudioFiles(rootDir);
			queue.setFinnished(true);
		}
	}

	/**
	 * rekursive suche nach audiodateien aus rootDir heraus. Ergebnisse werden
	 * in arraylist<file> audioFiles gespeichert
	 * 
	 * @param rootDir
	 */
	private synchronized void grabAudioFiles(File dir) {
		queue= new TrackToDBQueue();
		try {
			Files.walkFileTree(dir.toPath(),

			new SimpleFileVisitor<Path>() {

				@Override
				public FileVisitResult visitFileFailed(Path file,
						IOException exc) {
					return FileVisitResult.CONTINUE;
				}

				@Override
				public FileVisitResult visitFile(Path path,
						BasicFileAttributes attribs) {
					try {
						String mime = Files.probeContentType(path);
						if (null == mime) {
							return FileVisitResult.CONTINUE;
						}
						if (mime.startsWith("audio/")) {
							AudioFile af = AudioFileIO.read(path.toFile());
							Tag tag = af.getTag();
							//System.out.println("###### Lied: "+path);
							// default-werte, falls tags unvollständig.
							String artist = "NULL";
							String album = "NULL";
							String title = "NULL";
							String track = "NULL";
							String genre = "NULL";
							try {
								track = (tag.getFirst(FieldKey.TRACK));
								if ("0".equals(track)) { //falls eingetragener track 0 ist, als null übernehmen
									track = "NULL";
								}
							} catch (NullPointerException e) {/*e.printStackTrace();*/}
							try {
								artist = tag.getFirst(FieldKey.ARTIST);
							} catch (NullPointerException e) {/*e.printStackTrace();*/}
							try {
								album = tag.getFirst(FieldKey.ALBUM);
							} catch (NullPointerException e) {/*e.printStackTrace()*/;}
							try {
								title = tag.getFirst(FieldKey.TITLE);
							} catch (NullPointerException e) {/*e.printStackTrace()*/;}
							try {
								genre = tag.getFirst(FieldKey.GENRE);
							} catch (NullPointerException e) {/*e.printStackTrace()*/;}
//							System.out.println("MIME: "+mime);
//							System.out.println("Track: "+track);
//							System.out.println("Artist: "+artist);
//							System.out.println("Album: "+album);
//							System.out.println("Title: "+title);
//							System.out.println("Genre: "+genre);
//							System.out.println("Length: "+af.getAudioHeader().getTrackLength());
							try {
								queue.put(new TrackDto(track, artist, album, title, genre, af.getAudioHeader().getTrackLength(), path));
							} catch (InterruptedException e) {
								e.printStackTrace();
							}
						}
					}

					catch (IOException e) {
					} catch (CannotReadException e) {
						e.printStackTrace();
					} catch (TagException e) {
						e.printStackTrace();
					} catch (ReadOnlyFileException e) {
						e.printStackTrace();
					} catch (InvalidAudioFrameException e) {
						e.printStackTrace();
					}

					return FileVisitResult.CONTINUE;

				}

			});
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
