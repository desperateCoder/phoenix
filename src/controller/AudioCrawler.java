package controller;

import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.FileVisitor;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.BasicFileAttributes;
/**
 * 
 * @author Artur Dawtjan
 *
 */
public class AudioCrawler implements FileVisitor<Path> {

	FolderImporter importer;
	
	
	public AudioCrawler() {
	}
	
	//######################-> FileVisitor <-##########################
	public FileVisitResult postVisitDirectory(Path dir, IOException exc)
			throws IOException {
		return FileVisitResult.CONTINUE;
	}

	public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs)
			throws IOException {
		return FileVisitResult.CONTINUE;
	}

	public FileVisitResult visitFile(Path file, BasicFileAttributes attrs)
			throws IOException {

		try {
			String mime = Files.probeContentType(file);
			if (mime!=null && mime.startsWith("audio/")) {
				System.out.println("Audio: "+file);
				
			}
		}

		catch (IOException e) {
		}

		return FileVisitResult.CONTINUE;

	}

	public FileVisitResult visitFileFailed(Path file, IOException exc)
			throws IOException {

		if (exc != null)
			throw exc;
		return FileVisitResult.CONTINUE;
	}
	//#######################################################################
	
	/**
	 * importiert einen Ordner rekursiv, pickt sich audio-Files raus und
	 * schreibt sie in die DB
	 */
	public void importFolder() {
		importer = new FolderImporter();
		Thread importThread = new Thread(importer);
		importThread.start();
	}
}
