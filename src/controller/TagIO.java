package controller;

import java.io.File;
import java.io.IOException;

import model.Track;
import model.TrackDto;

import org.jaudiotagger.audio.AudioFile;
import org.jaudiotagger.audio.AudioFileIO;
import org.jaudiotagger.audio.exceptions.CannotReadException;
import org.jaudiotagger.audio.exceptions.CannotWriteException;
import org.jaudiotagger.audio.exceptions.InvalidAudioFrameException;
import org.jaudiotagger.audio.exceptions.ReadOnlyFileException;
import org.jaudiotagger.tag.FieldKey;
import org.jaudiotagger.tag.Tag;
import org.jaudiotagger.tag.TagException;

/**
 * Diese Klasse kontrolliert das lesen und schreiben von Tags
 * @author artur
 *
 */
public class TagIO {
	/**
	 * Schreibt die Tags des übergebenen tracks in die Datei. den Pfad bekommt es aus dem track, dieser muss also im track enthalten sein.
	 * @param t zu taggender Track
	 * @throws CannotReadException
	 * @throws IOException
	 * @throws TagException
	 * @throws ReadOnlyFileException
	 * @throws InvalidAudioFrameException
	 * @throws CannotWriteException
	 */
	public static void writeTags(Track t) throws CannotReadException, IOException, TagException, ReadOnlyFileException, InvalidAudioFrameException, CannotWriteException{
		
		File f = new File(t.getPath().toString());
		AudioFile af = AudioFileIO.read(f);
		
		Tag tag = af.getTag();
		tag.setField(FieldKey.ARTIST,t.getArtistName());
		tag.setField(FieldKey.ALBUM,t.getAlbumName());
		tag.setField(FieldKey.TITLE,t.getTitle());
		tag.setField(FieldKey.GENRE,t.getGenre());
		try {
			Integer.parseInt(t.getTrack());
			tag.setField(FieldKey.TRACK,t.getTrack());
		} catch (NumberFormatException e) {}
		tag.setField(FieldKey.ARTIST,t.getArtistName());
		af.commit();
	}
	
	/**
	 * Liest die Tags aus 
	 * @param t
	 * @return Track mit, welches die eingelesenen tags beinhaltet.
	 * @throws CannotReadException
	 * @throws IOException
	 * @throws TagException
	 * @throws ReadOnlyFileException
	 * @throws InvalidAudioFrameException
	 */
	public static Track readTags(Track t) throws CannotReadException, IOException, TagException, ReadOnlyFileException, InvalidAudioFrameException{
		Track track = null;
		File f = new File(t.getPath().toString());
		AudioFile af = AudioFileIO.read(f);
		
		Tag tag = af.getTag();
		String artist = tag.getFirst(FieldKey.ARTIST);
		String album = tag.getFirst(FieldKey.ALBUM);
		String title = tag.getFirst(FieldKey.TITLE);
		String nr = tag.getFirst(FieldKey.TRACK);
		String genre = tag.getFirst(FieldKey.GENRE);
		track = new TrackDto(nr, artist, album, title, genre, 0, t.getPath());
		return track;
	}
}
