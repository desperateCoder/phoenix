package controller.soundEngine;

import java.util.ArrayList;
import java.util.List;

import javax.swing.BoundedRangeModel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import model.Track;
import controller.Jukebox;
import controller.interfaces.JukeboxChangedTrackListener;
import controller.listener.AudioPositionListener;
import controller.soundEngine.lib.VLCJ;

public class PlayerAbstractionLayer implements BoundedRangeModel, AudioPositionListener{
	
	private Player currentPlayer=null;
	List<Player> player = new ArrayList<Player>();
	Jukebox master;
	private int currentVol = 50;
	private boolean isPaused = false;
	
	public PlayerAbstractionLayer(Jukebox master, JukeboxChangedTrackListener core) {
		this.master = master;
		initPlayer(core);
	}

	
	private void initPlayer(JukeboxChangedTrackListener core) {
		player.add(new VLCJ(core, this, this));
//		player.add(new GST(core, this, this));
//		player.add(new MP3(core, this, this));
//		player.add(new WAV(core, this, this));
	}
	


	private Player getRightPlayerForTrack(Track t) {
		for (Player p : player) {
			if (p.canPlay(t)) {
				return p;
			}
		}
		return null;
	}

	private boolean planB(Track t, int volume) {
		for (Player p : player) {
			if (p.tryPlayingTrack(t, volume)) {
				currentPlayer=p;
				isPaused=false;
				return true;
			}
		}
		return false;
	}
	
	public boolean playTrack(Track t, int volume) {
		currentPlayer = getRightPlayerForTrack(t);
		setVolume(volume);
		setRangeProperties(0, t.getLength(), 0, t.getLength(), false);
		if (currentPlayer==null) {
			return planB(t, volume);
		}else {
			currentPlayer.playTrack(t, volume);
			isPaused = false;
			return true;
		}
	}



	public void setVolume(int percent) {
		currentVol = percent;
		if (currentPlayer!=null) {
			currentPlayer.setVolume(currentVol);
		}
		
	}

	
	public void seek(int sec) {
		currentPlayer.seek(sec);
	}

	
	public void resume() {
		currentPlayer.resume();
		isPaused=false;
	}

	
	public void stop() {
		currentPlayer.stop();
	}

	
	public void pause() {
		currentPlayer.pause();
		isPaused=true;
	}

	
	public void play() {
		if (isPaused) {
			resume();
			isPaused = false;
			return;
		}
		isPaused = false;
		setVolume(currentVol);
		currentPlayer.play();
	}
	
	public void audioPositionChanged(int sec) {
		val=sec;
		notifyChangeListeners();
	}
	
	public void finnishedPlayingTrack(){
		master.finnishedPlayingTrack();
	}
	//--------------boundedRange-Model-----------------------

	private int min=0;
	private int max=0;
	private int val=0;
	private List<ChangeListener> changeListener = new ArrayList<ChangeListener>();
	private ChangeEvent evt = new ChangeEvent(this);
	public int getMinimum() {
		return min;
	}


	public void setMinimum(int newMinimum) {
		min=newMinimum;
		notifyChangeListeners();
	}


	public int getMaximum() {
		return max;
	}


	public void setMaximum(int newMaximum) {
		max = newMaximum;
		notifyChangeListeners();
	}


	public int getValue() {
		return val;
	}


	public void setValue(int newValue) {
		seek(newValue);
		notifyChangeListeners();
	}


	public void setValueIsAdjusting(boolean b) {
		return;
	}


	public boolean getValueIsAdjusting() {
		return false;
	}


	public int getExtent() {
		return 0;
	}


	public void setExtent(int newExtent) {/*nix*/}


	public void setRangeProperties(int value, int extent, int min, int max,
			boolean adjusting) {
		val = value;
		this.min = min;
		this.max = max;
		notifyChangeListeners();
	}

	private void notifyChangeListeners(){
		for (ChangeListener c : changeListener) {
			c.stateChanged(evt);
		}
	}
	
	public void addChangeListener(ChangeListener x) {
		changeListener.add(x);
	}

	public void removeChangeListener(ChangeListener x) {
		changeListener.remove(x);
	}
	//---------------------------------------------------------------------------
}
