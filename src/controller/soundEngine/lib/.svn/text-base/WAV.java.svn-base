package controller.soundEngine.lib;

//package org.nargila.util;

import java.io.File;
import java.io.IOException;
import java.net.URLConnection;
import java.nio.file.Files;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.FloatControl;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;

import model.Track;
import controller.interfaces.JukeboxChangedTrackListener;
import controller.listener.AudioPositionListener;
import controller.soundEngine.Player;
import controller.soundEngine.PlayerAbstractionLayer;

//import org.nargila.speak.framework.Conf;

/**
 * Simple wav player
 * 
 * @author Artur Dawtjan
 * 
 */
public class WAV extends Player {

	private Track track2Play;

	Thread locationThread;

	private Clip clip;

	/**
	 * @param l 
	 * @param p
	 * @throws UnsupportedAudioFileException
	 * @throws IOException
	 * @throws LineUnavailableException
	 */
	public WAV(JukeboxChangedTrackListener l, AudioPositionListener p, PlayerAbstractionLayer master){
		super(l, p, master);
		try {
			clip = AudioSystem.getClip();
		} catch (LineUnavailableException e1) {
			e1.printStackTrace();
		}
		locationThread = new Thread(new Runnable() {

			public void run() {
				while (true) {
					updateLocation();
					try {
						Thread.sleep(1000L);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}
		});

	}

	/**
	 * checkt, ob der clip schon offen ist
	 * 
	 * @return true, wenn schon offen
	 */
	private boolean isOpen() {
		return clip.isOpen();
	}

	/**
	 * Startet das abspielen.
	 * Vorsicht: der T
	 */
	public synchronized void play() {
		try {
			clip.start();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	
	@Override
	public boolean playTrack(Track t, int volume) {
		System.out.println("WAV");
		if (isOpen()) {
			stop();
		}
		setTrack2Play(t);
		setVolume(volume);
		play();
		if (!locationThread.isAlive()) {
			locationThread.start();
		}
		tweetToListeners(t);
		return true;
	}

	private void setTrack2Play(Track t) {
		AudioInputStream input;
		try {
			track2Play = t;
			input = AudioSystem.getAudioInputStream(track2Play.getPath()
					.toFile());
			clip.open(input);
		} catch (UnsupportedAudioFileException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (LineUnavailableException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void setVolume(int percent) {
		FloatControl volume = (FloatControl) clip
				.getControl(FloatControl.Type.MASTER_GAIN);
		float max = volume.getMaximum();
		float min = volume.getMinimum();
		float range = 0;
		if (max < 0) {
			range = Math.abs(min) - Math.abs(max);
		} else {
			if (min >= 0) {
				range = max - min;
			} else {
				range = Math.abs(min) + max;
			}
		}
		float value = percent * range / 100F;
		volume.setValue(min + value);
	}

	@Override
	public void seek(int sec) {
		clip.setMicrosecondPosition((long) sec * 1000000L);
		updateLocation();
	}

	@Override
	public void pause() {
		clip.stop();
		try {
			locationThread.wait();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void resume() {
		locationThread.notify();
		clip.start();
	}

	@Override
	public void stop() {
		clip.close();
		try {
			locationThread.wait();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	private void updateLocation() {
		long pos = clip.getMicrosecondPosition();
		tweetAudioPosition((int) ((pos - (pos % 1000000)) / 1000000));
	}

	@Override
	public boolean canPlay(Track t) {
		File f = t.getPath().toFile();
		String mimeType="";
		try {
			mimeType = Files.probeContentType(t.getPath());
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
		URLConnection.guessContentTypeFromName(f.getName());
		if(mimeType.equals("audio/wav")){
			return true;
		}else {
			return false;
		}
	}
}
