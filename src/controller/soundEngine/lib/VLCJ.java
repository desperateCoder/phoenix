package controller.soundEngine.lib;

import model.Track;
import uk.co.caprica.vlcj.binding.internal.libvlc_media_t;
import uk.co.caprica.vlcj.component.AudioMediaPlayerComponent;
import uk.co.caprica.vlcj.player.MediaPlayer;
import uk.co.caprica.vlcj.player.MediaPlayerEventListener;
import controller.interfaces.JukeboxChangedTrackListener;
import controller.listener.AudioPositionListener;
import controller.soundEngine.Player;
import controller.soundEngine.PlayerAbstractionLayer;

public class VLCJ extends Player implements MediaPlayerEventListener{
	AudioMediaPlayerComponent player;
	PlayerAbstractionLayer master;
	
	public VLCJ(JukeboxChangedTrackListener l, AudioPositionListener p, PlayerAbstractionLayer master) {
		super(l, p, master);
		
		this.master = master;
		
		player = new AudioMediaPlayerComponent();
		player.getMediaPlayer().addMediaPlayerEventListener(this);
	}

	private void updateLocation(long pos) {
		tweetAudioPosition((int) ((pos - (pos % 1000L)) / 1000L));
	}
	@Override
	public boolean playTrack(Track t, int volume) {
		player.getMediaPlayer().playMedia(t.getPath().toString());
		tweetToListeners(t);
		return false;
	}

	@Override
	public void setVolume(int percent) {
		player.getMediaPlayer().setVolume(percent);
	}

	@Override
	public void seek(int sec) {
		player.getMediaPlayer().setTime(sec*1000L);
	}

	@Override
	public void resume() {
		player.getMediaPlayer().play();
	}

	@Override
	public void stop() {
		player.getMediaPlayer().stop();
	}

	@Override
	public void pause() {
		player.getMediaPlayer().pause();
	}

	@Override
	public void play() {
		player.getMediaPlayer().play();
	}

	@Override
	public boolean canPlay(Track t) {
		return true;
	}

	//############## mediaplayereventlistener methods ####################
	@Override
	public void mediaChanged(MediaPlayer mediaPlayer, libvlc_media_t media,
			String mrl) {
	}

	@Override
	public void opening(MediaPlayer mediaPlayer) {
	}

	@Override
	public void buffering(MediaPlayer mediaPlayer, float newCache) {
	}

	@Override
	public void playing(MediaPlayer mediaPlayer) {
	}

	@Override
	public void paused(MediaPlayer mediaPlayer) {
	}

	@Override
	public void stopped(MediaPlayer mediaPlayer) {
	}

	@Override
	public void forward(MediaPlayer mediaPlayer) {
	}

	@Override
	public void backward(MediaPlayer mediaPlayer) {
	}

	@Override
	public void finished(MediaPlayer mediaPlayer) {
		master.finnishedPlayingTrack();
	}

	@Override
	public void timeChanged(MediaPlayer mediaPlayer, long newTime) {
		updateLocation(newTime);
	}

	@Override
	public void positionChanged(MediaPlayer mediaPlayer, float newPosition) {
	}

	@Override
	public void seekableChanged(MediaPlayer mediaPlayer, int newSeekable) {
	}

	@Override
	public void pausableChanged(MediaPlayer mediaPlayer, int newSeekable) {
	}

	@Override
	public void titleChanged(MediaPlayer mediaPlayer, int newTitle) {
	}

	@Override
	public void snapshotTaken(MediaPlayer mediaPlayer, String filename) {
	}

	@Override
	public void lengthChanged(MediaPlayer mediaPlayer, long newLength) {
	}

	@Override
	public void videoOutput(MediaPlayer mediaPlayer, int newCount) {
	}

	@Override
	public void error(MediaPlayer mediaPlayer) {
	}

	@Override
	public void mediaMetaChanged(MediaPlayer mediaPlayer, int metaType) {
	}

	@Override
	public void mediaSubItemAdded(MediaPlayer mediaPlayer,
			libvlc_media_t subItem) {
	}

	@Override
	public void mediaDurationChanged(MediaPlayer mediaPlayer, long newDuration) {
	}

	@Override
	public void mediaParsedChanged(MediaPlayer mediaPlayer, int newStatus) {
	}

	@Override
	public void mediaFreed(MediaPlayer mediaPlayer) {
	}

	@Override
	public void mediaStateChanged(MediaPlayer mediaPlayer, int newState) {
	}

	@Override
	public void newMedia(MediaPlayer mediaPlayer) {
	}

	@Override
	public void subItemPlayed(MediaPlayer mediaPlayer, int subItemIndex) {
	}

	@Override
	public void subItemFinished(MediaPlayer mediaPlayer, int subItemIndex) {
	}

	@Override
	public void endOfSubItems(MediaPlayer mediaPlayer) {
	}
}
