package controller.soundEngine.lib;

import java.util.concurrent.TimeUnit;

import model.Track;

import org.gstreamer.Bus.EOS;
import org.gstreamer.Bus.ERROR;
import org.gstreamer.ElementFactory;
import org.gstreamer.Gst;
import org.gstreamer.GstObject;
import org.gstreamer.State;
import org.gstreamer.elements.PlayBin2;

import controller.interfaces.JukeboxChangedTrackListener;
import controller.listener.AudioPositionListener;
import controller.soundEngine.Player;
import controller.soundEngine.PlayerAbstractionLayer;

public class GST extends Player implements EOS, ERROR {
	
	private PlayBin2 playbin;
	private Thread locationThread;
	public GST(JukeboxChangedTrackListener l, AudioPositionListener p, PlayerAbstractionLayer master) {
		super(l, p, master);
		Gst.init("AudioPlayerMetadata", new String[0]);
		playbin  = new PlayBin2("AudioPlayer");
		playbin.setVideoSink(ElementFactory.make("fakesink", "videosink"));
		playbin.getBus().connect((EOS)this);
	    playbin.getBus().connect((ERROR)this);
	    locationThread = new Thread(new Runnable() {

			public void run() {
				while (true) {
					updateLocation();
					try {
						Thread.sleep(1000L);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}

			
		});
	}
	
	private void updateLocation() {
		tweetAudioPosition((int)playbin.queryPosition().getSeconds());
	}
	
	@Override
	public boolean playTrack(Track t, int volume) {

		System.out.println("GST");
		if (playbin.getState() != State.NULL) {
			playbin.stop();
		}
			playbin.setInputFile(t.getPath().toFile());
			playbin.setVolumePercent(volume);
			playbin.play();
			if (!locationThread.isAlive()) {
				locationThread.start();
			}else {
				locationThread.notify();
			}
			
			tweetToListeners(t);
			return true;
	}

	@Override
	/**
	 * Setzt die lautstärke in prozent (0-100)
	 */
	public void setVolume(int percent){
		playbin.setVolumePercent(percent);
	}

	@Override
	public void pause() {
		playbin.pause();
		try {
			locationThread.wait();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	@Override
	public void play(){
		playbin.play();
		synchronized (locationThread) {
			locationThread.notify();
		}
		
	}
	/**
	 * Setzt den Player auf die stelle sec
	 * @param sec ab der gespielt werden soll
	 */
	@Override
	public void seek(int sec){//TODO will net so recht...
//		ClockTime ct = ClockTime.valueOf(sec*1000000000, TimeUnit.NANOSECONDS);
//		ClockTime ct = ClockTime.fromSeconds(((long)sec));
//		playbin.seek(1.0, Format.TIME, SeekFlags.FLUSH | SeekFlags.KEY_UNIT, 
//		            SeekType.SET, TimeUnit.SECONDS.convert(((long)sec), TimeUnit.NANOSECONDS), 
//		            SeekType.NONE, -1);

//		System.out.println("ct: "+ct.getSeconds());
		playbin.seek(sec, TimeUnit.SECONDS);
		System.out.println("sec: "+sec);
//		playbin.seek(sec, TimeUnit.SECONDS);
//		playbin.seek(ct);
		
	}

	@Override
	public void resume() {
		playbin.play();
		synchronized (locationThread) {
			locationThread.notify();
		}
	}

	@Override
	public void stop() {
		playbin.stop();
		try {
			locationThread.wait();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	@Override
	public boolean canPlay(Track t) {
		return true;
	}

	public void errorMessage(GstObject source, int code, String message) {
		System.out.println("Error occurred: " + message);
		try {
			locationThread.wait();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public void endOfStream(GstObject arg0) {
		finnishedPlayingTrack();
	}
}
