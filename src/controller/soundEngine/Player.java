package controller.soundEngine;

import java.util.ArrayList;

import controller.interfaces.JukeboxChangedTrackListener;
import controller.listener.AudioPositionListener;
import model.Track;

public abstract class Player {
	
	private ArrayList<JukeboxChangedTrackListener> trackChangedListener = new ArrayList<JukeboxChangedTrackListener>();
	private ArrayList<AudioPositionListener> positionListener = new ArrayList<AudioPositionListener>();
	protected PlayerAbstractionLayer master;
	/**
	 * Standard-constructor
	 * @param l Listener, der benachrichtigt wird, wenn sich der track ändert
	 * @param p Listener, der benachrichtigt wird, wenn sich die liedposition ändert
	 */
	public Player(JukeboxChangedTrackListener l, AudioPositionListener p, PlayerAbstractionLayer master) {
		this.trackChangedListener.add(l);
		positionListener.add(p);
		this.master = master;
	}
	/**
	 * Wechselt den Track
	 * @param t Track, der gespielt werden soll
	 */
	public abstract boolean playTrack(Track t, int volume);
	
	/**
	 * Setzt die lautstärke in prozent (0-100)
	 */
	public abstract void setVolume(int percent);
	
	/**
	 * Setzt den Player auf die stelle sec
	 * @param sec ab der gespielt werden soll
	 */
	public abstract void seek(int sec);
	
	public abstract void resume();
	
	public abstract void stop();
	
	public abstract void pause();
	
	public abstract void play();
	
	/**
	 * findet heraus, ob der Player den Track abspielen kann	
	 * @param t Track
	 * @return
	 */
	public abstract boolean canPlay(Track t);
	
	protected void finnishedPlayingTrack(){
		master.finnishedPlayingTrack();
	}
	/**
	 * Probiert, die datei abzuspielen
	 * @param t zu spielender Track
	 * @param volume Lautstärke
	 * @return true, wenn die datei abgespielt werden kann, false wenn nicht
	 */
	public boolean tryPlayingTrack(Track t, int volume){
		try {
			play();
		} catch (Exception e) {
			return false;
		}
		return true;
	}
	
	/**
	 * meldet den Listenern, welches lied gespielt wird.
	 * @param t
	 */
	protected void tweetToListeners(Track t){
		for (JukeboxChangedTrackListener l : trackChangedListener) {
			l.trackChanged(t);
		}
	}
	/**
	 * meldet den Listenern, an welcher posisiton des liedes die wiedergabe ist
	 * @param t
	 */
	protected void tweetAudioPosition(int sec){
		for (AudioPositionListener l : positionListener) {
			l.audioPositionChanged(sec);
		}
	}
	
	public void addJukeboxChangedTrackListener(JukeboxChangedTrackListener l){
		trackChangedListener.add(l);
	}
}
