package controller;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class SQLiteConnection {
	private Connection libCon = null;
	private Connection settingCon = null;
	private ResultSet resultSet = null;
	private Statement libStatement = null;
	private Statement settingStatement = null;

	public SQLiteConnection() {
		String userDir = System.getProperty("user.dir");
		String fs = File.separator;
		try {
			Class.forName("org.sqlite.JDBC");
			libCon = DriverManager.getConnection("jdbc:sqlite:" + userDir
					+ fs + "db" + fs + "lib");
			libStatement = libCon.createStatement();
			settingCon = DriverManager.getConnection("jdbc:sqlite:" + userDir
					+ fs + "db" + fs + "settings");
			settingStatement = settingCon.createStatement();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	/**
	 * nur für ein insert auf die LIB-DB gedacht. mehrere zeilen einfügen geht, es wird aber nur der key des ersten eintrags geliefert.
	 * @param ps Prepared Statement (Variablen müssen bereits bestückt sein)
	 * @return generierte ID in der DB
	 */
	public synchronized int insertOnLib(PreparedStatement ps) {
		try {
			ps.executeUpdate();
		} catch (SQLException e) {
			//e.printStackTrace(); //<-query does not return ResultSet
		}
		ResultSet tmp;
		try {
			tmp = ps.getGeneratedKeys();
			if (tmp.next()) {
				int id =tmp.getInt(1);
				tmp.close();
				return id;
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return 0;
		}
		return 0;
		
	}
	
	/**
	 * nur für ein insert auf die LIB-DB gedacht. mehrere zeilen einfügen geht, es wird aber nur der key des ersten eintrags geliefert.
	 * @param query
	 * @return generierte ID in der DB
	 */
	public synchronized int insertOnLib(String query) {
		try {
			libStatement.executeUpdate(query);
		} catch (SQLException e) {
			//e.printStackTrace(); //<-query does not return ResultSet
		}
		ResultSet tmp;
		try {
			tmp = libStatement.getGeneratedKeys();
			if (tmp.next()) {
			int id = tmp.getInt(1);
			tmp.close();
			return id;
		}
		} catch (SQLException e) {
			e.printStackTrace();
			return 0;
		}
		return 0;
		
	}
	/**
	 * Updates und selects auf die LIB-DB
	 * @param query
	 * @return das ResultSet
	 */
	public synchronized ResultSet executeOnLib(String query) {
		try {
			resultSet = libStatement.executeQuery(query);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return resultSet;
	}
	/**
	 * Updates und selects auf die SETTING-DB
	 * @param query
	 * @return das ResultSet
	 */
	public synchronized ResultSet executeOnSettings(String query) {
		try {
			resultSet = settingStatement.executeQuery(query);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return resultSet;
	}

	/**
	 * Trennt die Verbindung
	 */
	public void disconnectAll() {
		try {
			resultSet.close();
			libStatement.close();
			libCon.close();;
			settingStatement.close();
			settingCon.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public synchronized void deleteOnLib(String sql){
		try {
			libStatement.execute(sql);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	public PreparedStatement prepareOnLib(String sql) throws SQLException{
		return libCon.prepareStatement(sql);
	}
	public PreparedStatement prepareOnSettings(String sql) throws SQLException{
		return settingCon.prepareStatement(sql);
	}
	public void setLibAutoCommit(boolean isAutocommit){
		try {
			libCon.setAutoCommit(isAutocommit);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	public void commitLib() {
		try {
			libCon.commit();
		} catch (SQLException e) {
			try {
				libCon.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();
		}
	}

	public void rollbackLib() {
		try {
			libCon.rollback();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}