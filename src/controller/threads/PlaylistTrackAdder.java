package controller.threads;

import java.sql.PreparedStatement;
import java.sql.SQLException;

import model.Track;
import controller.PhoenixCore;

public class PlaylistTrackAdder implements Runnable {
	Track[] tracks;
	PreparedStatement addTracksToPlaylistStmt = null;
	int id;
	int start;
	/**
	 * dieser Thread befördert die übergebenen tracks in die DB
	 * @param dbCon Datenbankanbindung
	 * @param tracks, die eingefügt werden sollen
	 * @param id der Playlist
	 * @param start erster wert der spalte running_order, sollte die größe des libs sein.
	 */
	public PlaylistTrackAdder(Track[] tracks, int id, int start) {
		this.id = id;
		this.tracks = tracks;
		this.start = start;
		try {
			addTracksToPlaylistStmt = PhoenixCore.DBCON.prepareOnLib("insert into playlist_track (listid, trackid, running_order) values (?, ?, ?)");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void run() {
		addTracksToDB();
	}
	private void addTracksToDB() {
		try {
			addTracksToPlaylistStmt.setInt(1, id);
			Track track;
			for (int i = 0; i<tracks.length; i++) {
				track = tracks[i];
				addTracksToPlaylistStmt.setInt(2, track.getID());
				addTracksToPlaylistStmt.setInt(3, start+i);
				try {
					addTracksToPlaylistStmt.execute();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
				
	}
}
