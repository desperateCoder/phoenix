package controller.threads;

import java.sql.PreparedStatement;
import java.sql.SQLException;

import controller.PhoenixCore;

/**
 * Diese Klasse ist ein Thread, welcher die playlist
 * @author Artur Dawtjan
 *
 */
public class PlaylistRowReorderer implements Runnable{
	int size;
	int fromIndex;
	int toIndex;
	int libID;
	PreparedStatement ps = null;
	public PlaylistRowReorderer(int size, int libID, int from, int to) {
		this.size = size;
		fromIndex = from;
		this.libID = libID;
		toIndex = to;
		try {
			ps = PhoenixCore.DBCON.prepareOnLib("update playlist_track set running_order=? where running_order=? and listid=?");
		} catch (SQLException e) {
			e.printStackTrace();
		}
		PhoenixCore.DBCON.setLibAutoCommit(false);
	}
	
	public void run() {
		try {
			reorder();
		} catch (SQLException e) {
			PhoenixCore.DBCON.rollbackLib();
			e.printStackTrace();
		}
	}
	private void reorder() throws SQLException{
		int tmpID = size+10;
		ps.setInt(1, tmpID);
		ps.setInt(2, fromIndex);
		ps.setInt(3, libID);
		ps.executeUpdate();
		if (fromIndex<toIndex) {//nach unten verschoben...
			for (int i = fromIndex; i < toIndex-1; i++) {
				ps.setInt(1, i);
				ps.setInt(2, i+1);
				ps.executeUpdate();
			}
			ps.setInt(1, toIndex-1);
			ps.setInt(2, tmpID);
			ps.executeUpdate();
		}else {//nach oben verschoben...
			for (int i = fromIndex; i > toIndex&&i>0; i--) {
				ps.setInt(1, i);
				ps.setInt(2, i-1);
				ps.executeUpdate();
			}
			ps.setInt(1, toIndex);
			ps.setInt(2, tmpID);
			ps.executeUpdate();
		}
		PhoenixCore.DBCON.commitLib();
	}
}
