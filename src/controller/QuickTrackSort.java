package controller;

import model.Library;
import model.Track;

public class QuickTrackSort {
	public static void sort(Library x, char columnConstant) {
		qSort(x, 0, x.getTrackCount() - 1, columnConstant);
	}

	private static void qSort(Library x, int links, int rechts, char columnConstant) {
		if (links < rechts) {
			int i = partition(x, links, rechts, columnConstant);
			qSort(x, links, i - 1, columnConstant);
			qSort(x, i + 1, rechts, columnConstant);
		}
	}

	private static int partition(Library x, int links, int rechts, char columnConstant) {
		int i, j;
		Track temp;
		Track pivot= x.getTrackAt(rechts);
		i = links;
		j = rechts - 1;
		while (i <= j) {
			if (x.getTrackAt(i).compareTo(pivot, columnConstant)) {
				// tausche x[i] und x[j]
				temp = x.getTrackAt(i);
				x.setTrackAt(i, x.getTrackAt(j));
				x.setTrackAt(j, temp);
				j--;
			} else
				i++;
		}
		// tausche x[i] und x[rechts]
		temp = x.getTrackAt(i);
		x.setTrackAt(i, x.getTrackAt(rechts));
		x.setTrackAt(rechts, temp);

		return i;
	}
}