package controller;

import java.util.ArrayList;

import model.Track;

import org.gstreamer.Bus;
import org.gstreamer.ClockTime;
import org.gstreamer.ElementFactory;
import org.gstreamer.Gst;
import org.gstreamer.Pipeline;
import org.gstreamer.State;
import org.gstreamer.elements.PlayBin2;

import controller.interfaces.JukeboxChangedTrackListener;

public class Player {
	private PlayBin2 playbin;
	private ArrayList<JukeboxChangedTrackListener> trackChangedListener = new ArrayList<JukeboxChangedTrackListener>();
	public Player(Bus.EOS jukebox,Bus.ERROR jb, JukeboxChangedTrackListener l){
		trackChangedListener.add(l);
		Gst.init("AudioPlayerMetadata", new String[0]);
		playbin  = new PlayBin2("AudioPlayer");
		playbin.setVideoSink(ElementFactory.make("fakesink", "videosink"));
		playbin.getBus().connect(jukebox);
	    playbin.getBus().connect(jb);
	}
	  
	/**
	 * Wechselt den Track
	 * @param t Track, der gespielt werden soll
	 */
	public boolean playTrack(Track t, int volume){
		if (playbin.getState() != State.NULL) {
			playbin.stop();
		}
			playbin.setInputFile(t.getPath().toFile());
			playbin.setVolumePercent(volume);
			playbin.play();
			tweetToListeners(t);
			return true;
	}
	
	/**
	 * meldet den Listenern, welches lied gespielt wird.
	 * @param t
	 */
	private void tweetToListeners(Track t){
		for (JukeboxChangedTrackListener l : trackChangedListener) {
			l.trackChanged(t);
		}
	}
	
	/**
	 * Setzt die lautstärke in prozent (0-100)
	 */
	public void setVolume(int percent){
		playbin.setVolumePercent(percent);
	}

	public void addJukeboxChangedTrackListener(JukeboxChangedTrackListener l){
		trackChangedListener.add(l);
	}
	public void pause() {
		playbin.pause();
	}
	public void play(){
		playbin.play();
	}
	/**
	 * Setzt den Player auf die stelle sec
	 * @param sec ab der gespielt werden soll
	 */
	public void seek(int sec){
		playbin.seek(ClockTime.fromSeconds(sec));
	}

	public Pipeline getPipe() {
		return playbin;
	}

	public void stop() {
		playbin.stop();
	}
}
