package controller.interfaces;

import model.Track;

public interface JukeboxChangedTrackListener {
	public void trackChanged(Track t);
}
