package controller.interfaces;

public interface PlayModeChangedListener {
	public void modeChanged(int newMode);
}
