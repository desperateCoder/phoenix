package controller.interfaces;

public interface JukeboxStateListener {
	public static final int PAUSED=0;
	public static final int PLAYING=1;
	
	public void jukeboxStateChanged(int state);
}
