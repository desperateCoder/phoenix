package controller.interfaces.customs;

import java.awt.Component;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.dnd.DropTarget;
import java.awt.dnd.DropTargetDragEvent;
import java.awt.dnd.DropTargetDropEvent;
import java.awt.dnd.DropTargetEvent;
import java.awt.dnd.DropTargetListener;
import java.io.File;
import java.util.List;
import java.util.TooManyListenersException;

import model.dto.TrackTransferDto;

@SuppressWarnings("serial")
public class FileDropTarget extends DropTarget{
	Component source = null;
	public FileDropTarget(Component c) {
		super(c, null);
		source=c;
		DropTargetListener dropTargetListener = new DropTargetListener() {

		// Die Maus betritt die Komponente mit
		// einem Objekt
		public void dragEnter(DropTargetDragEvent e) {
			//evtl statuszeile text setzen
		}

		// Die Komponente wird verlassen
		public void dragExit(DropTargetEvent e) {
			//evtl text in status zuruecksetzen
		}

		// Die Maus bewegt sich über die Komponente
		public void dragOver(DropTargetDragEvent e) {
		}

		public void drop(DropTargetDropEvent e) {
			try {
				Transferable tr = e.getTransferable();
				DataFlavor[] flavors = tr.getTransferDataFlavors();
				boolean isFileFlavour=false;
				boolean isDtoFlavour=false;
				DataFlavor fileFlavour=null;
				for (int i = 0; i < flavors.length; i++){
					if (flavors[i].isFlavorJavaFileListType()) {
						isFileFlavour = true;
						fileFlavour = flavors[i];
					}else if (flavors[i].equals(new DataFlavor(DataFlavor.javaJVMLocalObjectMimeType +
												";class=\""+ TrackTransferDto.class.getName() + "\""))) {
						isDtoFlavour = true;
					}
				}
				if (isFileFlavour&&!isDtoFlavour) {
					// Zunächst annehmen
					e.acceptDrop(e.getDropAction());
					@SuppressWarnings("unchecked")
					List<File> files = (List<File>) tr.getTransferData(fileFlavour);
					
					for (int j = 0; j < files.size(); j++) {
						File f =  files.get(j);
						System.out.println(f.getAbsolutePath());
						//TODO ordner / dateien importieren
	
					}
					e.dropComplete(true);
					return;
				}
				
			} catch (Throwable t) {
				t.printStackTrace();
			}
			// Ein Problem ist aufgetreten
			e.rejectDrop();
		}

		// Jemand hat die Art des Drops (Move, Copy, Link)
		// geändert
		public void dropActionChanged(DropTargetDragEvent e) {
		}
	};
	try {
		addDropTargetListener(dropTargetListener);
	} catch (TooManyListenersException e1) {
		e1.printStackTrace();
	}
	}
	
}
