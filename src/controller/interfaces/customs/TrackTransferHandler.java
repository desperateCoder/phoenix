package controller.interfaces.customs;

import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;

import javax.swing.JComponent;
import javax.swing.TransferHandler;

import model.Track;

@SuppressWarnings("serial")
public class TrackTransferHandler extends TransferHandler {
	/**
	 * This method is used to query what actions are supported by the source
	 * component, such as COPY, MOVE, or LINK, in any combination. For example,
	 * a customer list might not support moving a customer name out of the list,
	 * but it would very likely support copying the customer name. Most of our
	 * examples support both COPY and MOVE.
	 */
	@Override
	public int getSourceActions(JComponent c) {
		return COPY; //nur kopieren, nicht anschließend entfrernen oder so
	}

	/**
	 * This method bundles up the data to be exported into a Transferable object
	 * in preparation for the transfer.
	 */
	@Override
	protected Transferable createTransferable(JComponent c) {
		return null;
	}

	/**
	 * This method is called repeatedly during a drag gesture and returns true
	 * if the area below the cursor can accept the transfer, or false if the
	 * transfer will be rejected. For example, if a user drags a color over a
	 * component that accepts only text, the canImport method for that
	 * component's TransferHandler should return false.
	 */
	@Override
	public boolean canImport(TransferSupport support) {
		support.isDataFlavorSupported(new DataFlavor(Track[].class, "Tracks Flavor"));
		return super.canImport(support);
	}

	/**
	 * This method is called on a successful drop (or paste) and initiates the
	 * transfer of data to the target component. This method returns true if the
	 * import was successful and false otherwise.
	 */
	@Override
	public boolean importData(JComponent comp, Transferable t) {
		return super.importData(comp, t);
	}

	/**
	 * wird aufgerufen, wenn export fertig ist (evtl aufräumen, wenn rows
	 * rausgezogen werden oder so)
	 */
	@Override
	protected void exportDone(JComponent source, Transferable data, int action) {
		super.exportDone(source, data, action);
	}

	/**
	 * This method is called repeatedly during a drag gesture and returns true
	 * if the area below the cursor can accept the transfer, or false if the
	 * transfer will be rejected. For example, if a user drags a color over a
	 * component that accepts only text, the canImport method for that
	 * component's TransferHandler should return false.
	 */
	@Override
	public boolean canImport(JComponent comp, DataFlavor[] transferFlavors) {
		return super.canImport(comp, transferFlavors);
	}

	/**
	 * This method is called on a successful drop (or paste) and initiates the
	 * transfer of data to the target component. This method returns true if the
	 * import was successful and false otherwise.
	 */
	@Override
	public boolean importData(TransferSupport support) {
		return super.importData(support);
	}
}
