package controller.interfaces;

import javax.swing.JFrame;

import model.Library;
import model.Track;
import view.AbstractView;
import controller.PhoenixCore;

/**
 * Interface für skins. skin-klassen müssen diese implementieren.
 * 
 * @author Artur Dawtjan
 * 
 */
@SuppressWarnings("serial")
public abstract class PhoenixGUI extends JFrame {
	/**
	 * setzt die lautstärke auf 0
	 */
	public abstract void mute();
	/**
	 * setzt die lautstärke auf volle möhre
	 */
	public abstract void maxVolume();
	/**
	 * liefert die aktuelle lautstärke
	 * @return aktuelle lautstärke
	 */
	public abstract int getVolume();
	/**
	 * der status der jukebox hat sich geändert.
	 * @param state neuer status
	 */
	public abstract void jukeboxStateChanged(int state);
	
	/**
	 * ein Liedwechsel hat stattgefunden
	 * @param t	 neuer track
	 * @param index row-index in der tabelle
	 */
	public abstract void trackChanged(Track t, int index);
	
	/**
	 * zeigt die übergebenen lyrics im lyric-Bereich an.
	 * @param lyrix
	 */
	public abstract void setLyrix(String lyrix);
	/**
	 * liefert die aktuell angezeigte bibliothek
	 * @return Library aus aktuell angezeigter view
	 */
	public abstract Library getCurrentPlayedLib();
	/**
	 * Liefert den kernel
	 * @return referenz auf core
	 */
	public abstract PhoenixCore getCore();
	/**
	 * setzt die View in das View-Panel
	 * @param abstractView
	 */
	public abstract void setView(AbstractView abstractView);
	/**
	 * Aktuelle View wird nun abgespielt. 
	 */
	public abstract void currentViewPlayed();
	/**
	 * @return Die Queue-library
	 */
	public abstract Library getCurrentQueueLib();
	/**
	 * aktualisiert die queue-view. daten neu einlesen und so
	 */
	public abstract void updateQueueView();
	/**
	 * Bibliothek (alle lieder!) werden neu aus der DB gelesen
	 */
	public abstract void refreshLibrary();
}
