package controller.interfaces;

public interface QuickSearchable {
 public void quickSearch(String text);
}
