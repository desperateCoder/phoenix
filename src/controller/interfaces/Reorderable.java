package controller.interfaces;

public interface Reorderable {
	   public void reorder(int fromIndex, int toIndex);
}
