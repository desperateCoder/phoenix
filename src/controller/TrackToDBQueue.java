package controller;

import java.util.concurrent.LinkedBlockingQueue;

import model.Track;

/**
 * eine queue, die nach und nach den stapel verarbeitet - first in - frist out
 * der verbraucher, TracktoDBwriter, wird im konstruktor gestartet.
 * @author Artur Dawtjan
 * 
 */
@SuppressWarnings("serial")
public class TrackToDBQueue extends LinkedBlockingQueue<Track>{
	Track track;
	private boolean finnished = false;
	private Thread writer = null;
	/**
	 * 
	 * @param con, datenbankverbindung
	 */
	public TrackToDBQueue() {
		writer = new Thread(new TracktoDBwriter(this));
		writer.setPriority(Thread.MIN_PRIORITY);
		writer.start();
	}
	
	public boolean hasFinnished(){
		return finnished;
	}

	public void setFinnished(boolean b) {
		finnished=b;
		if (finnished) {
			writer.setPriority(Thread.NORM_PRIORITY);
		} else {
			writer.setPriority(Thread.MIN_PRIORITY);
		}
	}
//
//	synchronized Track get() {
//		if (!valueSet)
//			try {
//				wait();
//			} catch (InterruptedException e) {
//				System.out.println("InterruptedException caught");
//			}
//		valueSet = false;
//		notify();
//		return track;
//	}
	

//	synchronized void put(Track t) {
//		if (valueSet)
//			try {
//				wait();
//			} catch (InterruptedException e) {
//				System.out.println("InterruptedException caught");
//			}
//		this.track = t;
//		valueSet = true;
//		notify();
//	}

}
