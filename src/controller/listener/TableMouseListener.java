package controller.listener;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JTable;

import model.AbstractTrackTableModel;
import controller.PhoenixCore;

/**
 * Listener für sämtliche maus-aktionen im TrackTable
 * 
 * @author Artur Dawtjan
 * 
 */
public class TableMouseListener implements MouseListener {
	PhoenixCore core;
	public TableMouseListener(PhoenixCore core){
		this.core = core;
	}
	public void mouseClicked(MouseEvent e) {
		//Doppelklick auf ein Lied: abspielen!
		if (e.getClickCount() == 2) {
			JTable target = (JTable) e.getSource();
			int row = target.getSelectedRow();
			AbstractTrackTableModel model = ((AbstractTrackTableModel)target.getModel());
			core.fillJukebox(model.getLib(), row);
		}
	}

	public void mouseEntered(MouseEvent e) {

	}

	public void mouseExited(MouseEvent e) {

	}

	public void mousePressed(MouseEvent e) {

	}

	public void mouseReleased(MouseEvent e) {

	}

}
