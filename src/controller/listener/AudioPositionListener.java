package controller.listener;


public interface AudioPositionListener {
	public void audioPositionChanged(int sec);
}
