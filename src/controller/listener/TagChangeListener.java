package controller.listener;

import java.util.List;

import model.Track;

public interface TagChangeListener {
	public void tagsChanged(List<Track> tracks, int selectionIndex[]);
}
