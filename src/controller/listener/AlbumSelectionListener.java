package controller.listener;

import javax.swing.JTable;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import model.Album;
import view.views.LibView;

public class AlbumSelectionListener implements ListSelectionListener {
	JTable table;
	LibView view;
	
	public AlbumSelectionListener(JTable tab, LibView v) {
		table = tab;
		view = v;
	}
	public void valueChanged(ListSelectionEvent e) {
		if (!e.getValueIsAdjusting()) {
			int row = table.getSelectedRow();
			if (row>=0) {
				Album selection = ((Album)table.getModel().getValueAt(row, 0));
				view.albumSelected(selection);
			}
			
		}
	}

}
