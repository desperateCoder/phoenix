package controller.listener;

import javax.swing.JTable;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import model.Artist;

import view.views.LibView;
/**
 * Diese klasse handhabt die selktion vom interpret in der lib-view
 * @author Artur Dawtjan
 *
 */
public class ArtistSelectionListener implements ListSelectionListener {
	JTable table;
	LibView view;
	
	public ArtistSelectionListener(JTable tab, LibView v) {
		table = tab;
		view = v;
	}
	public void valueChanged(ListSelectionEvent e) {
		if (!e.getValueIsAdjusting()) {
			int row =table.getSelectedRow();
			if (row>=0) {
				Artist selection = ((Artist) table.getModel().getValueAt(row, 0));
				view.artistSelected(selection);
			}
		}
	}

}
