package controller.listener;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.JSplitPane;

import controller.PhoenixCore;

public class DividerLocationPropertyChangeListener implements PropertyChangeListener {
		private int settingKey;
		private int waitTime=0;
		private JSplitPane split;
		private Runnable waitingThread = new Runnable() {
			public void run() {
//				System.out.println("thread "+settingKey+" started!");
				// warten, bis benutzer fertig rezized hat.
				while (waitTime > 0) {
					waitTime -= 100;
					try {
						Thread.sleep(100);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
				PhoenixCore.INSTANCE.getSettings().setSetting(settingKey, split.getDividerLocation()+"");
//				System.out.println("value "+ settingKey + " written");
			}
		};
		/**
		 * Konstruktor fuer Splitpanes (dividerLocation)
		 * @param settingKey
		 * @param split
		 */
		public DividerLocationPropertyChangeListener(int settingKey, JSplitPane split) {
			this.settingKey = settingKey;
			this.split = split;
		}
		
		/**
		 * Konstruktor fuer components, (ComponentListener, position + size)
		 * @param settingKey
		 */
		public DividerLocationPropertyChangeListener(int settingKey) {
			this.settingKey = settingKey;
		}
		@Override
		public void propertyChange(PropertyChangeEvent pce) {
			if (waitTime<1) {
				waitTime = 1000;
				new Thread(waitingThread).start();
			} else {
				waitTime = 1000;
			}
		}
}
