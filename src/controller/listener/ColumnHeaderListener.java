package controller.listener;

import java.awt.Rectangle;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.table.TableColumnModel;

import view.TrackTable;

public class ColumnHeaderListener extends MouseAdapter {
	TrackTable table;
	public ColumnHeaderListener(TrackTable trackTable) {
		table = trackTable;
	}
    public void mouseClicked(MouseEvent evt) {
        TableColumnModel colModel = table.getColumnModel();

        // The index of the column whose header was clicked
        int vColIndex = colModel.getColumnIndexAtX(evt.getX());
        int mColIndex = table.convertColumnIndexToModel(vColIndex);

        // Return if not clicked on any column header
        if (vColIndex == -1) {
            return;
        }

        // Determine if mouse was clicked between column heads
        Rectangle headerRect = table.getTableHeader().getHeaderRect(vColIndex);
        if (vColIndex == 0) {
            headerRect.width -= 3;    // Hard-coded constant
        } else {
            headerRect.grow(-3, 0);   // Hard-coded constant
        }
        if (!headerRect.contains(evt.getX(), evt.getY())) {
            // Mouse was clicked between column heads
            // vColIndex is the column head closest to the click

//            // vLeftColIndex is the column head to the left of the click
//            int vLeftColIndex = vColIndex;
//            if (evt.getX() < headerRect.x) {
//                vLeftColIndex--;
//            }
        }
        table.sortColumnAt(vColIndex, mColIndex);
    }
}