package controller;

import controller.interfaces.PhoenixGUI;
import controller.interfaces.customs.HttpLyricWikiBackend;

/**
 * besorgt die lyrics und meldet es 
 * @author Artur Dawtjan
 *
 */
public class LyrixCrawler implements Runnable{
	String artist, title;
	PhoenixGUI gui;
	public LyrixCrawler(String artist, String title, PhoenixGUI gui) {
		this.artist = artist;
		this.title = title;
		this.gui = gui;
	}
	private String getLyrix(){
		return HttpLyricWikiBackend.getLyrix(artist, title);
	}
	public void run() {
		gui.setLyrix(getLyrix());
	}
	
}
