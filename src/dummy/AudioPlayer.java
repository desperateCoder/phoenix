package dummy;

import java.io.File;

import org.gstreamer.Bus;
import org.gstreamer.ElementFactory;
import org.gstreamer.Gst;
import org.gstreamer.GstObject;
import org.gstreamer.State;
import org.gstreamer.TagList;
import org.gstreamer.elements.PlayBin2;

public class AudioPlayer {
	public static void main(String[] args) {
		args = Gst.init("AudioPlayerMetadata", args);
		PlayBin2 playbin = new PlayBin2("AudioPlayer");
		playbin.setVideoSink(ElementFactory.make("fakesink", "videosink"));
		playbin.setInputFile(new File("/home/artur/Musik/Kram-Musik/Misfits Alben/Misfits.10 ALBEN/Famous Monsters/16 Hunting Humans The Misfits Famous Monsters Rock 192kbps.wma"));

		playbin.getBus().connect(new Bus.TAG() {

			public void tagsFound(GstObject source, TagList tagList) {
				for (String tagName : tagList.getTagNames()) {
					for (Object tagData : tagList.getValues(tagName)) {
						if (!"".equals(tagData)) {
							System.out.printf("%s: %s\n", tagName, tagData);
						}
					}
				}
			}
		});
		playbin.play();
//		playbin.pause();
//		playbin.seek(ClockTime.fromSeconds(5));
//		playbin.play();
		Gst.main();
		playbin.setState(State.NULL);
	}
}